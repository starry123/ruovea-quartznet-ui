﻿using RuoVea.ExEnum;

namespace RuoVea.QuartzNetUI.Server.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class TaskLogQueryDto
    {
        /// <summary>
        /// 任务主键 
        /// </summary>
        public long? JobId { get; set; }

        /// <summary>
        /// 任务名称
        /// </summary>
        public string JobName { get; set; }

        /// <summary>
        /// 任务分组
        /// </summary>
        public string JobGroup { get; set; }
        /// <summary>
        /// 是否启动
        /// </summary>
        public YesOrNot? Status { get; set; }
    }
}

