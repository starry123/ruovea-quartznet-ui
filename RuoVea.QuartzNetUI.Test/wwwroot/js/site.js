﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
// 多语言文本对象
var i18n = {
    "en": {
        "welcome": "Welcome to our website!"
    },
    "es": {
        "welcome": "Bienvenido a nuestro sitio web!"
    },
    "fr": {
        "welcome": "Bienvenue sur notre site web!"
    }
};

// jQuery插件定义
$.fn.i18n = function (key) {
    return this.text(i18n[window.navigator.language || "en"][key] || "");
};

// 使用插件
$(document).ready(function () {
    $("#welcome-message").i18n("welcome");
});