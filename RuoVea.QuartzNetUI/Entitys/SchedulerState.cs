﻿using SqlSugar;
namespace RuoVea.QuartzNetUI.Entitys
{
    /// <summary>
    /// 存储集群中note实例信息，quartz会定时读取该表的信息判断集群中每个实例的当前状态
    ///</summary>
    [SugarTable("Qrtz_Scheduler_State", tableDescription: "存储集群中note实例信息")]
    public class SchedulerState
    {
        /// <summary>
        /// 调度名称 
        ///</summary>
        [SugarColumn(ColumnName = "Sched_Name", Length = 120, IsPrimaryKey = true, ColumnDescription = "调度名称")]
        public string SchedName { get; set; }
        /// <summary>
        /// 之前配置文件中org.quartz.scheduler.instanceId配置的名字，就会写入该字段 
        ///</summary>
        [SugarColumn(ColumnName = "Instance_Name", Length = 200, IsPrimaryKey = true, ColumnDescription = "之前配置文件中org.quartz.scheduler.instanceId配置的名字，就会写入该字段 ")]
        public string InstanceName { get; set; }
        /// <summary>
        /// 上次检查时间 
        ///</summary>
        [SugarColumn(ColumnName = "Last_Checkin_Time", Length = 19, ColumnDescription = "上次检查时间")]
        public long LastCheckinTime { get; set; }
        /// <summary>
        /// 检查间隔时间 
        ///</summary>
        [SugarColumn(ColumnName = "Checkin_Interval",Length=19, ColumnDescription = "检查间隔时间")]
        public long CheckinInterval { get; set; }
    }
}
