﻿using RuoVea.ExDto;
using RuoVea.QuartzNetUI.Entitys;
using RuoVea.QuartzNetUI.Server.Dto;
using System.Linq.Expressions;

namespace RuoVea.QuartzNetUI.Server
{
    /// <summary>
    /// 任务调度详情 
    /// </summary>
    public interface ITaskDetailService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        Task<bool> AnyAsync(Expression<Func<TaskDetail, bool>> expression);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        Task<RestfulResult<TaskDetail>> GetByIdAsync(long Id);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<RestfulResult> InsertAsync(TaskDetail data);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<RestfulResult> UpdateAsync(TaskDetail data);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<RestfulResult> DeleteAsync(TaskDetaiDeleteDto data);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<RestfulResult<IList<TaskDetailDto>>> GetTaskDetailListAsync(TaskDetailQueryDto data);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<RestfulResult<PageResult<TaskDetailDto>>> GetTaskDetailPageListAsync(TaskDetailPageDto data);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        TaskDetail DtoToDetail(TaskDetailDto data);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        TaskDetail InDtoToDetail(TaskDetailInDto data);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<RestfulResult> SaveSetting(TaskSettingDto data);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<RestfulResult> GetSettingByCode(string data);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        TaskDetailDto DetailToDto(TaskDetail data);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        TaskDetailInDto DetailToInDto(TaskDetail data);
    }
}
