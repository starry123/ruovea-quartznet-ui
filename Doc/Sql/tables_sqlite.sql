drop table if exists Qrtz_Fired_Triggers; 
drop table if exists Qrtz_Paused_Trigger_Grps;
drop table if exists Qrtz_Scheduler_State;
drop table if exists Qrtz_Locks;
drop table if exists Qrtz_Simprop_Triggers;
drop table if exists Qrtz_Simple_Triggers;
drop table if exists Qrtz_Cron_Triggers;
drop table if exists Qrtz_Blob_Triggers;
drop table if exists Qrtz_Triggers;
drop table if exists Qrtz_Job_Details;
drop table if exists Qrtz_Calendars;


create table Qrtz_Job_Details
  (
    Sched_Name nvarchar(120) not null,
	Job_Name nvarchar(150) not null,
    Job_Group nvarchar(150) not null,
    Description nvarchar(250) null,
    Job_Class_Name   nvarchar(250) not null,
    Is_Durable bit not null,
    Is_Nonconcurrent bit not null,
    Is_Update_Data bit  not null,
	Requests_Recovery bit not null,
    Job_Data blob null,
    primary key (Sched_Name,Job_Name,Job_Group)
);

create table Qrtz_Triggers
  (
    Sched_Name nvarchar(120) not null,
	Trigger_Name nvarchar(150) not null,
    Trigger_Group nvarchar(150) not null,
    Job_Name nvarchar(150) not null,
    Job_Group nvarchar(150) not null,
    Description nvarchar(250) null,
    Next_Fire_Time bigint null,
    Prev_Fire_Time bigint null,
    Priority integer null,
    Trigger_State nvarchar(16) not null,
    Trigger_Type nvarchar(8) not null,
    Start_Time bigint not null,
    End_Time bigint null,
    Calendar_Name nvarchar(200) null,
    Misfire_Instr integer null,
    Job_Data blob null,
    primary key (Sched_Name,Trigger_Name,Trigger_Group),
    foreign key (Sched_Name,Job_Name,Job_Group)
        references Qrtz_Job_Details(Sched_Name,Job_Name,Job_Group)
);

create table Qrtz_Simple_Triggers
  (
    Sched_Name nvarchar(120) not null,
	Trigger_Name nvarchar(150) not null,
    Trigger_Group nvarchar(150) not null,
    Repeat_Count bigint not null,
    Repeat_Interval bigint not null,
    Times_Triggered bigint not null,
    primary key (Sched_Name,Trigger_Name,Trigger_Group),
    foreign key (Sched_Name,Trigger_Name,Trigger_Group)
        references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group) on delete cascade
);

create trigger delete_simple_trigger delete on Qrtz_Triggers
begin
	delete from Qrtz_Simple_Triggers where Sched_Name=old.Sched_Name and Trigger_Name=old.Trigger_Name and Trigger_Group=old.Trigger_Group;
end
;

create table Qrtz_Simprop_Triggers 
  (
    Sched_Name nvarchar (120) not null ,
    Trigger_Name nvarchar (150) not null ,
    Trigger_Group nvarchar (150) not null ,
    Str_Prop_1 nvarchar (512) null,
    Str_Prop_2 nvarchar (512) null,
    Str_Prop_3 nvarchar (512) null,
    Int_Prop_1 int null,
    Int_Prop_2 int null,
    Long_Prop_1 bigint null,
    Long_Prop_2 bigint null,
    Dec_Prop_1 numeric null,
    Dec_Prop_2 numeric null,
    Bool_Prop_1 bit null,
    Bool_Prop_2 bit null,
    Time_Zone_Id nvarchar(80) null,
	primary key (Sched_Name,Trigger_Name,Trigger_Group),
	foreign key (Sched_Name,Trigger_Name,Trigger_Group)
        references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group) on delete cascade
);

create trigger delete_simprop_trigger delete on Qrtz_Triggers
begin
	delete from Qrtz_Simprop_Triggers where Sched_Name=old.Sched_Name and Trigger_Name=old.Trigger_Name and Trigger_Group=old.Trigger_Group;
end
;

create table Qrtz_Cron_Triggers
  (
    Sched_Name nvarchar(120) not null,
	Trigger_Name nvarchar(150) not null,
    Trigger_Group nvarchar(150) not null,
    Cron_Expression nvarchar(250) not null,
    Time_Zone_Id nvarchar(80),
    primary key (Sched_Name,Trigger_Name,Trigger_Group),
    foreign key (Sched_Name,Trigger_Name,Trigger_Group)
        references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group) on delete cascade
);

create trigger delete_cron_trigger delete on Qrtz_Triggers
begin
	delete from Qrtz_Cron_Triggers where Sched_Name=old.Sched_Name and Trigger_Name=old.Trigger_Name and Trigger_Group=old.Trigger_Group;
end
;

create table Qrtz_Blob_Triggers
  (
    Sched_Name nvarchar(120) not null,
	Trigger_Name nvarchar(150) not null,
    Trigger_Group nvarchar(150) not null,
    Blob_Data blob null,
    primary key (Sched_Name,Trigger_Name,Trigger_Group),
    foreign key (Sched_Name,Trigger_Name,Trigger_Group)
        references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group) on delete cascade
);

create trigger delete_blob_trigger delete on Qrtz_Triggers
begin
	delete from Qrtz_Blob_Triggers where Sched_Name=old.Sched_Name and Trigger_Name=old.Trigger_Name and Trigger_Group=old.Trigger_Group;
end
;

create table Qrtz_Calendars
  (
    Sched_Name nvarchar(120) not null,
	Calendar_Name  nvarchar(200) not null,
    Calendar blob not null,
    primary key (Sched_Name,Calendar_Name)
);

create table Qrtz_Paused_Trigger_Grps
  (
    Sched_Name nvarchar(120) not null,
	Trigger_Group nvarchar(150) not null, 
    primary key (Sched_Name,Trigger_Group)
);

create table Qrtz_Fired_Triggers
  (
    Sched_Name nvarchar(120) not null,
	entry_id nvarchar(140) not null,
    Trigger_Name nvarchar(150) not null,
    Trigger_Group nvarchar(150) not null,
    Instance_Name nvarchar(200) not null,
    Fired_Time bigint not null,
    Sched_Time bigint not null,
	Priority integer not null,
    State nvarchar(16) not null,
    Job_Name nvarchar(150) null,
    Job_Group nvarchar(150) null,
    Is_Nonconcurrent bit null,
    Requests_Recovery bit null,
    primary key (Sched_Name,entry_id)
);

create table Qrtz_Scheduler_State
  (
    Sched_Name nvarchar(120) not null,
	Instance_Name nvarchar(200) not null,
    Last_Checkin_Time bigint not null,
    Checkin_Interval bigint not null,
    primary key (Sched_Name,Instance_Name)
);

create table Qrtz_Locks
  (
    Sched_Name nvarchar(120) not null,
	Lock_Name  nvarchar(40) not null, 
    primary key (Sched_Name,Lock_Name)
);
