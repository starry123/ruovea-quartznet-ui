﻿namespace RuoVea.QuartzNetUI.Server.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class TaskSettingDto
    {
        /// <summary>
        /// 主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 分组代码 
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 邮箱 
        /// </summary>
        public string EmailAccount { get; set; }

        /// <summary>
        /// 邮箱密码 
        /// </summary>
        public string EmaillPass { get; set; }

    }
}
