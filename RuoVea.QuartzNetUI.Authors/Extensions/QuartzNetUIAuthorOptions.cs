﻿namespace RuoVea.QuartzNetUI.Authors
{
    /// <summary>
    /// 
    /// </summary>
    public class QuartzNetUIAuthorOption : QuartzNetUIOption
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string UserPwd { get; set; }

        /// <summary>
        /// 登录校验路径
        /// </summary>
        public string AuthorUrl { get; set; }
    }
}
