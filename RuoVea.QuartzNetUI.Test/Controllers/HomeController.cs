﻿using Microsoft.AspNetCore.Mvc;
using RuoVea.QuartzNetUI.Test.Models;
using SqlSugar;
using System.Diagnostics;

namespace RuoVea.QuartzNetUI.Test.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ISqlSugarClient _sqlSugarClient;

        public HomeController(ILogger<HomeController> logger,ISqlSugarClient sqlSugarClient)
        {
            _sqlSugarClient= sqlSugarClient;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpGet]
        public string Initdb()
        {
            string body = "drop table if exists Qrtz_Fired_Triggers; \r\ndrop table if exists Qrtz_Paused_Trigger_Grps;\r\ndrop table if exists Qrtz_Scheduler_State;\r\ndrop table if exists Qrtz_Locks;\r\ndrop table if exists Qrtz_Simprop_Triggers;\r\ndrop table if exists Qrtz_Simple_Triggers;\r\ndrop table if exists Qrtz_Cron_Triggers;\r\ndrop table if exists Qrtz_Blob_Triggers;\r\ndrop table if exists Qrtz_Triggers;\r\ndrop table if exists Qrtz_Job_Details;\r\ndrop table if exists Qrtz_Calendars;\r\n\r\n\r\ncreate table Qrtz_Job_Details\r\n  (\r\n    Sched_Name nvarchar(120) not null,\r\n\tJob_Name nvarchar(150) not null,\r\n    Job_Group nvarchar(150) not null,\r\n    Description nvarchar(250) null,\r\n    Job_Class_Name   nvarchar(250) not null,\r\n    Is_Durable bit not null,\r\n    Is_Nonconcurrent bit not null,\r\n    Is_Update_Data bit  not null,\r\n\tRequests_Recovery bit not null,\r\n    Job_Data blob null,\r\n    primary key (Sched_Name,Job_Name,Job_Group)\r\n);\r\n\r\ncreate table Qrtz_Triggers\r\n  (\r\n    Sched_Name nvarchar(120) not null,\r\n\tTrigger_Name nvarchar(150) not null,\r\n    Trigger_Group nvarchar(150) not null,\r\n    Job_Name nvarchar(150) not null,\r\n    Job_Group nvarchar(150) not null,\r\n    Description nvarchar(250) null,\r\n    Next_Fire_Time bigint null,\r\n    Prev_Fire_Time bigint null,\r\n    Priority integer null,\r\n    Trigger_State nvarchar(16) not null,\r\n    Trigger_Type nvarchar(8) not null,\r\n    Start_Time bigint not null,\r\n    End_Time bigint null,\r\n    Calendar_Name nvarchar(200) null,\r\n    Misfire_Instr integer null,\r\n    Job_Data blob null,\r\n    primary key (Sched_Name,Trigger_Name,Trigger_Group),\r\n    foreign key (Sched_Name,Job_Name,Job_Group)\r\n        references Qrtz_Job_Details(Sched_Name,Job_Name,Job_Group)\r\n);\r\n\r\ncreate table Qrtz_Simple_Triggers\r\n  (\r\n    Sched_Name nvarchar(120) not null,\r\n\tTrigger_Name nvarchar(150) not null,\r\n    Trigger_Group nvarchar(150) not null,\r\n    Repeat_Count bigint not null,\r\n    Repeat_Interval bigint not null,\r\n    Times_Triggered bigint not null,\r\n    primary key (Sched_Name,Trigger_Name,Trigger_Group),\r\n    foreign key (Sched_Name,Trigger_Name,Trigger_Group)\r\n        references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group) on delete cascade\r\n);\r\n\r\ncreate trigger delete_simple_trigger delete on Qrtz_Triggers\r\nbegin\r\n\tdelete from Qrtz_Simple_Triggers where Sched_Name=old.Sched_Name and Trigger_Name=old.Trigger_Name and Trigger_Group=old.Trigger_Group;\r\nend\r\n;\r\n\r\ncreate table Qrtz_Simprop_Triggers \r\n  (\r\n    Sched_Name nvarchar (120) not null ,\r\n    Trigger_Name nvarchar (150) not null ,\r\n    Trigger_Group nvarchar (150) not null ,\r\n    Str_Prop_1 nvarchar (512) null,\r\n    Str_Prop_2 nvarchar (512) null,\r\n    Str_Prop_3 nvarchar (512) null,\r\n    Int_Prop_1 int null,\r\n    Int_Prop_2 int null,\r\n    Long_Prop_1 bigint null,\r\n    Long_Prop_2 bigint null,\r\n    Dec_Prop_1 numeric null,\r\n    Dec_Prop_2 numeric null,\r\n    Bool_Prop_1 bit null,\r\n    Bool_Prop_2 bit null,\r\n    Time_Zone_Id nvarchar(80) null,\r\n\tprimary key (Sched_Name,Trigger_Name,Trigger_Group),\r\n\tforeign key (Sched_Name,Trigger_Name,Trigger_Group)\r\n        references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group) on delete cascade\r\n);\r\n\r\ncreate trigger delete_simprop_trigger delete on Qrtz_Triggers\r\nbegin\r\n\tdelete from Qrtz_Simprop_Triggers where Sched_Name=old.Sched_Name and Trigger_Name=old.Trigger_Name and Trigger_Group=old.Trigger_Group;\r\nend\r\n;\r\n\r\ncreate table Qrtz_Cron_Triggers\r\n  (\r\n    Sched_Name nvarchar(120) not null,\r\n\tTrigger_Name nvarchar(150) not null,\r\n    Trigger_Group nvarchar(150) not null,\r\n    Cron_Expression nvarchar(250) not null,\r\n    Time_Zone_Id nvarchar(80),\r\n    primary key (Sched_Name,Trigger_Name,Trigger_Group),\r\n    foreign key (Sched_Name,Trigger_Name,Trigger_Group)\r\n        references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group) on delete cascade\r\n);\r\n\r\ncreate trigger delete_cron_trigger delete on Qrtz_Triggers\r\nbegin\r\n\tdelete from Qrtz_Cron_Triggers where Sched_Name=old.Sched_Name and Trigger_Name=old.Trigger_Name and Trigger_Group=old.Trigger_Group;\r\nend\r\n;\r\n\r\ncreate table Qrtz_Blob_Triggers\r\n  (\r\n    Sched_Name nvarchar(120) not null,\r\n\tTrigger_Name nvarchar(150) not null,\r\n    Trigger_Group nvarchar(150) not null,\r\n    Blob_Data blob null,\r\n    primary key (Sched_Name,Trigger_Name,Trigger_Group),\r\n    foreign key (Sched_Name,Trigger_Name,Trigger_Group)\r\n        references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group) on delete cascade\r\n);\r\n\r\ncreate trigger delete_blob_trigger delete on Qrtz_Triggers\r\nbegin\r\n\tdelete from Qrtz_Blob_Triggers where Sched_Name=old.Sched_Name and Trigger_Name=old.Trigger_Name and Trigger_Group=old.Trigger_Group;\r\nend\r\n;\r\n\r\ncreate table Qrtz_Calendars\r\n  (\r\n    Sched_Name nvarchar(120) not null,\r\n\tCalendar_Name  nvarchar(200) not null,\r\n    Calendar blob not null,\r\n    primary key (Sched_Name,Calendar_Name)\r\n);\r\n\r\ncreate table Qrtz_Paused_Trigger_Grps\r\n  (\r\n    Sched_Name nvarchar(120) not null,\r\n\tTrigger_Group nvarchar(150) not null, \r\n    primary key (Sched_Name,Trigger_Group)\r\n);\r\n\r\ncreate table Qrtz_Fired_Triggers\r\n  (\r\n    Sched_Name nvarchar(120) not null,\r\n\tentry_id nvarchar(140) not null,\r\n    Trigger_Name nvarchar(150) not null,\r\n    Trigger_Group nvarchar(150) not null,\r\n    Instance_Name nvarchar(200) not null,\r\n    Fired_Time bigint not null,\r\n    Sched_Time bigint not null,\r\n\tPriority integer not null,\r\n    State nvarchar(16) not null,\r\n    Job_Name nvarchar(150) null,\r\n    Job_Group nvarchar(150) null,\r\n    Is_Nonconcurrent bit null,\r\n    Requests_Recovery bit null,\r\n    primary key (Sched_Name,entry_id)\r\n);\r\n\r\ncreate table Qrtz_Scheduler_State\r\n  (\r\n    Sched_Name nvarchar(120) not null,\r\n\tInstance_Name nvarchar(200) not null,\r\n    Last_Checkin_Time bigint not null,\r\n    Checkin_Interval bigint not null,\r\n    primary key (Sched_Name,Instance_Name)\r\n);\r\n\r\ncreate table Qrtz_Locks\r\n  (\r\n    Sched_Name nvarchar(120) not null,\r\n\tLock_Name  nvarchar(40) not null, \r\n    primary key (Sched_Name,Lock_Name)\r\n);\r\n";
           int resi= _sqlSugarClient.Ado.ExecuteCommand(body);
            return resi+"";
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}