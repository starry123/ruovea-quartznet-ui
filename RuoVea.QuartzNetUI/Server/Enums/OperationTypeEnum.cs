﻿namespace RuoVea.QuartzNetUI.Server.Enums
{
    /// <summary>
    /// 
    /// </summary>
    public enum OperationTypeEnum
    {
        /// <summary>
        /// 
        /// </summary>
        StopJob = 0,
        /// <summary>
        /// 
        /// </summary>
        RemoveJob = 1,
        /// <summary>
        /// 
        /// </summary>
        ResumeJob = 2,
        /// <summary>
        /// 
        /// </summary>
        Run = 3
    }
}

