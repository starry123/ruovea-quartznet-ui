﻿using SqlSugar;
namespace RuoVea.QuartzNetUI.Entitys
{
    /// <summary>
    /// 以 Blob 类型存储存放日历信息， quartz可配置一个日历来指定一个时间范围
    ///</summary>
    [SugarTable("Qrtz_Calendars", tableDescription: "以Blob类型存储存放日历信息")]
    public class Calendars
    {
        /// <summary>
        /// 调度名称 
        ///</summary>
        [SugarColumn(ColumnName = "Sched_Name", Length = 120, IsPrimaryKey = true, ColumnDescription = "调度名称")]
        public string SchedName { get; set; }
        /// <summary>
        /// 日历名称 
        ///</summary>
        [SugarColumn(ColumnName = "Calendar_Name", Length = 200, IsPrimaryKey = true, ColumnDescription = "日历名称")]
        public string CalendarName { get; set; }
        /// <summary>
        /// 一个blob字段，存放持久化calendar对象 
        ///</summary>
        [SugarColumn(ColumnName = "Calendar", ColumnDescription = "一个blob字段，存放持久化calendar对象")]
        public byte[] Calendar { get; set; }
    }
}
