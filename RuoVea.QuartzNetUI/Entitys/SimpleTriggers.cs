﻿using SqlSugar;
namespace RuoVea.QuartzNetUI.Entitys
{
    /// <summary>
    /// 存储简单的 Trigger，包括重复次数，间隔，以及已触发的次数
    ///</summary>
    [SugarTable("Qrtz_Simple_Triggers", tableDescription: "存储简单的Trigger")]
    public class SimpleTriggers
    {
        /// <summary>
        /// 调度名称 
        ///</summary>
        [SugarColumn(ColumnName = "Sched_Name", Length = 120, IsPrimaryKey = true, ColumnDescription = "调度名称")]
        public string SchedName { get; set; }
        /// <summary>
        /// qrtz_triggers表trigger_ name的外键 
        ///</summary>
        [SugarColumn(ColumnName = "Trigger_Name", Length = 150, IsPrimaryKey = true, ColumnDescription = "qrtz_triggers表trigger_ name的外键")]
        public string TriggerName { get; set; }
        /// <summary>
        /// qrtz_triggers表trigger_group的外键 
        ///</summary>
        [SugarColumn(ColumnName = "Trigger_Group", Length = 150, IsPrimaryKey = true, ColumnDescription = "qrtz_triggers表trigger_group的外键")]
        public string TriggerGroup { get; set; }
        /// <summary>
        /// 重复的次数统计 
        ///</summary>
        [SugarColumn(ColumnName = "Repeat_Count", Length = 7, ColumnDescription = "重复的次数统计")]
        public long RepeatCount { get; set; }
        /// <summary>
        /// 重复的间隔时间 
        ///</summary>
        [SugarColumn(ColumnName = "Repeat_Interval", Length = 12, ColumnDescription = "重复的间隔时间")]
        public long RepeatInterval { get; set; }
        /// <summary>
        /// 已经触发的次数 
        ///</summary>
        [SugarColumn(ColumnName = "Times_Triggered", Length = 10, ColumnDescription = "已经触发的次数")]
        public long TimesTriggered { get; set; }
    }
}
