﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.Options;

namespace RuoVea.QuartzNetUI.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class QuartzNetUIRouteAttribute :  ApiControllerAttribute,IApiDescriptionGroupNameProvider, IRouteTemplateProvider
    {
        /// <summary>
        /// 
        /// </summary>
        public virtual string Template =>RuoVea.ExApp.App.GetRequiredService<IOptions<QuartzNetUIOption>>()?.Value?.RootUrl?.ToString()??"Job";
        /// <summary>
        /// 
        /// </summary>
        public virtual int? Order => 2;
        /// <summary>
        /// 
        /// </summary>
        public virtual string Name =>string.Empty;
        /// <summary>
        /// 
        /// </summary>
        public virtual string GroupName => RuoVea.ExApp.App.GetRequiredService<IOptions<QuartzNetUIOption>>()?.Value?.GroupName?.ToString() ?? "QuartzNetUI";
    }
}