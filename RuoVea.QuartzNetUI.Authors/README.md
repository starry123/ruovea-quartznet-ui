# RuoVea.QuartzNetUI.Authors

#### 介绍

 QuartzNet UI Authors界面

#### 安装教程

1.  builder.Services.AddQuartzNetAuthorServer(builder.Configuration.GetSection("QuartzNetUI")); 

2.  app.UseQuartzAuthorUI();


#### 数据库表创建-执行脚本地址

https://gitee.com/starry123/ruovea-quartznet-ui/tree/develop/Doc/Sql

#### 使用说明

1.  builder.Services.AddQuartzNetAuthorServer(builder.Configuration.GetSection("QuartzNetUI")); 

2.  app.UseQuartzAuthorUI(); 

SQLite 引用  System.Data.SQLite  


#### 配置文件增加

```
"QuartzNetUI": {

  "DbType": "SQLite", //MySql、SqlServer、PostgreSQL、SQLite、Oracle、Firebird(后续支持)
  "ConnectionString": "Data Source=./quartznetui.db"

  //"DbType": "MySql", //MySql、SqlServer、PostgreSQL、SQLite、Oracle、Firebird(后续支持)
  //"ConnectionString": "server=localhost;Database=quartznetui;Uid=root;Pwd=haosql",

  //"DbType": "SqlServer", //MySql、SqlServer、PostgreSQL、SQLite、Oracle、Firebird(后续支持)
  //"ConnectionString": "server=.;uid=sa;pwd=haosql;database=quartznetui",

  //"DbType": "PostgreSQL", //MySql、SqlServer、PostgreSQL、SQLite、Oracle、Firebird(后续支持)
  //"ConnectionString": "PORT=5432;DATABASE=quartznetui;HOST=localhost;PASSWORD=haosql;USER ID=postgres",

  //"DbType": "Oracle", //MySql、SqlServer、PostgreSQL、SQLite、Oracle、Firebird(后续支持)
  //"ConnectionString": "Data Source=localhost/quartznetui;User ID=system;Password=haha"

  "RootUrl":"job",
  "IsClustered":false,//开启集群默认 false  集群不支持SQLite数据库
  "SchedulerId":"RuoVeaSchedulerId-001",//集群时使用
  "ShowMenu": true, //显示菜单
  "LogoUrl": "", //图标地址
  "UiTitle": "", //Ui标题

  "UserName":"admin",
  "UserPwd":"admin",
  "AuthorUrl":"job"//登录校验路径 默认RootUrl的值job
}

```