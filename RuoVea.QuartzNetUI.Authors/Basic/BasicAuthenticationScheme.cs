﻿namespace RuoVea.QuartzNetUI.Authors.Basic
{
    /// <summary>
    /// 常量
    /// </summary>
    public static class BasicAuthenticationScheme
    {
        /// <summary>
        /// 常量值
        /// </summary>
        public const string DefaultScheme = "Basic";
    }
}
