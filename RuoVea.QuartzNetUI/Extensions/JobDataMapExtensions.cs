﻿using Quartz;
using RuoVea.ExEnum;
using RuoVea.ExUtil;
using RuoVea.QuartzNetUI.Server.Dto;
using RuoVea.QuartzNetUI.Server.Enums;

namespace RuoVea.QuartzNetUI
{
    /// <summary>
    /// 扩展
    /// </summary>
    public static class JobDataMapExtensions
    {
        #region RequestUrl
        /// <summary>
        /// 设置请求url
        /// </summary>
        /// <param name="map"></param>
        /// <param name="data"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public static void SetRequestUrl(this JobDataMap map, string data)
        {
            if (data.NotNullOrWhiteSpace())
            {
                KeyValuePair<string, object> keyValuePairs = new KeyValuePair<string, object>("RequestUrl", data);
                map.Add(keyValuePairs);
            }
        }
        /// <summary>
        /// 获取请求url
        /// </summary>
        /// <param name="map"></param>
        /// <returns></returns>
        public static string GetRequestUrl(this JobDataMap map) => map.GetString("RequestUrl") ?? string.Empty; 
        #endregion

        #region Parameters
        /// <summary>
        /// 设置 请求参数（Post，Put请求用） {"Authorization":"userpassword.."}
        /// </summary>
        /// <param name="map"></param>
        /// <param name="data"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public static void SetParameters(this JobDataMap map, string data)
        {
            if (data.NotNullOrWhiteSpace())
            {
                KeyValuePair<string, object> keyValuePairs = new KeyValuePair<string, object>("Parameters", data);
                map.Add(keyValuePairs);
            }
        }
        /// <summary>
        ///  获取 请求参数（Post，Put请求用） {"Authorization":"userpassword.."}
        /// </summary>
        /// <param name="map"></param>
        /// <returns></returns>
        public static string GetParameters(this JobDataMap map) => map.GetString("Parameters") ?? string.Empty; 
        #endregion

        #region Headers
        /// <summary>
        /// 设置 Headers(可以包含如：Authorization授权认证) 格式：{"Authorization":"userpassword.."}
        /// </summary>
        /// <param name="map"></param>
        /// <param name="data"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public static void SetHeaders(this JobDataMap map, string data)
        {
            if (data.NotNullOrWhiteSpace())
            {
                KeyValuePair<string, object> keyValuePairs = new KeyValuePair<string, object>("Headers", data);
                map.Add(keyValuePairs);
            }
        }
        /// <summary>
        /// 获取 Headers(可以包含如：Authorization授权认证) 格式：{"Authorization":"userpassword.."}
        /// </summary>
        /// <param name="map"></param>
        /// <returns></returns>
        public static string GetHeaders(this JobDataMap map) => map.GetString("Headers") ?? string.Empty;
        #endregion

        #region LastException
        /// <summary>
        /// 设置 异常
        /// </summary>
        /// <param name="map"></param>
        /// <param name="data"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public static void SetLastException(this JobDataMap map, string data)
        {
            if (data.NotNullOrWhiteSpace())
            {
                KeyValuePair<string, object> keyValuePairs = new KeyValuePair<string, object>("LastException", data);
                map.Add(keyValuePairs);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static string GetLastException(this JobDataMap map) => map.GetString("LastException") ?? string.Empty;
        #endregion

        #region RunType
        /// <summary>
        /// 设置 异常
        /// </summary>
        /// <param name="map"></param>
        /// <param name="data"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public static void SetRunType(this JobDataMap map, RequestTypeEnum data)
        {
            KeyValuePair<string, object> keyValuePairs = new KeyValuePair<string, object>("RunType",data.ToString());
            map.Add(keyValuePairs);
        }
        /// <summary>
        /// 
        /// </summary>
        public static RequestTypeEnum GetRunType(this JobDataMap map) {
            var runType = map["RunType"];
            switch (runType)
            {
                case "Get": return RequestTypeEnum.Get;
                case "Post": return RequestTypeEnum.Post;
                case "Put": return RequestTypeEnum.Put;
                case "Delete": return RequestTypeEnum.Delete;
                default: return RequestTypeEnum.Run;
            }
        }
        #endregion

        #region JobData
        /// <summary>
        /// 设置 异常
        /// </summary>
        /// <param name="map"></param>
        /// <param name="data"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public static void SetJobData(this JobDataMap map, TaskDetailInDto data)
        {
            if (data!=null)
            {
                KeyValuePair<string, object> keyValuePairs = new KeyValuePair<string, object>("JobData", data);
                map.Add(keyValuePairs);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static TaskDetailInDto GetJobData(this JobDataMap map) => (map["JobData"] as TaskDetailInDto) ?? new TaskDetailInDto();
        #endregion
 
        #region MailMsgType
        /// <summary>
        /// 设置 发送类型
        /// </summary>
        /// <param name="map"></param>
        /// <param name="data"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public static void SetMailMsgType(this JobDataMap map, SendMsgTypeEnum data)
        {
            KeyValuePair<string, object> keyValuePairs = new KeyValuePair<string, object>("MailMsgType", data);
            map.Add(keyValuePairs);
        }
        /// <summary>
        /// 发送类型
        /// </summary>
        /// <param name="map"></param>
        /// <returns></returns>
        public static SendMsgTypeEnum GetMailMsgType(this JobDataMap map) => (map["MailMsgType"] as SendMsgTypeEnum?) ?? SendMsgTypeEnum.None;
        #endregion

        #region SendMailers
        /// <summary>
        /// 设置邮件接收者
        /// </summary>
        /// <param name="map"></param>
        /// <param name="data"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public static void SetSendMailers(this JobDataMap map, List<string> data)
        {
            if (data!=null&&data.Count>0)
            {
                KeyValuePair<string, object> keyValuePairs = new KeyValuePair<string, object>("SendMailers", data);
                map.Add(keyValuePairs);
            }
        }
        /// <summary>
        /// 邮件接收者
        /// </summary>
        /// <param name="map"></param>
        /// <returns></returns>
        public static List<string> GetSendMailers(this JobDataMap map) =>  (map["SendMailers"] as List<string>) ?? new List<string>();
        #endregion


    }
}
