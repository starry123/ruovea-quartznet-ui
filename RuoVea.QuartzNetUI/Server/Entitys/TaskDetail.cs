﻿using RuoVea.ExEnum;
using RuoVea.QuartzNetUI.Server.Enums;
using SqlSugar;

namespace RuoVea.QuartzNetUI.Entitys
{
    /// <summary>
    /// 任务调度详情用于再次重启加载使用
    /// </summary>
    [SugarTable("Qrtz_Task_Detail", tableDescription: "任务调度详情用于再次重启加载使用")]
    public class TaskDetail 
    {
        /// <summary>
        /// 主键
        /// </summary>
        [SugarColumn(ColumnName = "Id", IsPrimaryKey = true, ColumnDescription = "主键")]
        public long Id { get; set; }

        /// <summary>
        /// 调度名称 
        /// </summary>
        [SugarColumn(ColumnName = "Sched_Name", Length = 120, ColumnDescription = "调度名称")]
        public string SchedName { get; set; }

        /// <summary>
        /// 任务名称
        /// </summary>
        [SugarColumn(ColumnName = "Job_Name", ColumnDescription = "任务名称")]
        public string JobName { get; set; }

        /// <summary>
        /// 任务组名称
        /// </summary>
        [SugarColumn(ColumnName = "Job_Group", ColumnDescription = "任务组名称")]
        public string JobGroup { get; set; }

        /// <summary>
        /// @I18n.StartTime
        /// </summary>
        [SugarColumn(ColumnName = "Begin_Time", ColumnDescription = "@I18n.StartTime")]
        public DateTime? BeginTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        [SugarColumn(ColumnName = "End_Time", ColumnDescription = "结束时间")]
        public DateTime? EndTime { get; set; } = DateTime.MaxValue;

        /// <summary>
        /// Cron表达式
        /// </summary>
        [SugarColumn(ColumnName = "Cron", ColumnDescription = "Cron表达式")]
        public string Cron { get; set; }

        /// <summary>
        /// 执行次数（默认无限循环）
        /// </summary>
        [SugarColumn(ColumnName = "Run_Times", ColumnDescription = "执行次数（默认无限循环）")]
        public int? RunTimes { get; set; }

        /// <summary>
        /// 执行间隔时间，单位秒（如果有Cron，则IntervalSecond失效）
        /// </summary>
        [SugarColumn(ColumnName = "Interval_Second", IsNullable = true, ColumnDescription = "执行间隔时间，单位秒（如果有Cron，则IntervalSecond失效）")]
        public int? IntervalSecond { get; set; }

        /// <summary>
        /// 调度方式
        /// </summary>
        [SugarColumn(ColumnName = "Schedule_Type", ColumnDescription = "调度方式")]
        public ScheduleTypeEnum? ScheduleType { get; set; }

        /// <summary>
        /// 触发器类型
        /// </summary>
        [SugarColumn(ColumnName = "Trigger_Type", ColumnDescription = "触发器类型")]
        public TriggerTypeEnum? TriggerType { get; set; } = TriggerTypeEnum.Simple;

        /// <summary>
        /// 运行类型
        /// </summary>
        [SugarColumn(ColumnName = "Run_Type", ColumnDescription = "运行类型")]
        public RequestTypeEnum? RunType { get; set; } = RequestTypeEnum.Get;

        ///// <summary>
        ///// 时间间隔
        ///// </summary>
        //[SugarColumn(ColumnName = "Interval", ColumnDescription = "时间间隔")]
        //public string Interval { get; set; }

        #region Api请求参数
        /// <summary>
        /// 请求url
        /// </summary>
        [SugarColumn(ColumnName = "Request_Url", IsNullable = true, ColumnDescription = "请求url")]
        public string RequestUrl { get; set; }

        /// <summary>
        /// Headers(可以包含如：Authorization授权认证)
        /// 格式：{"Authorization":"userpassword.."}
        /// </summary>
        [SugarColumn(ColumnName = "Headers", IsNullable = true, ColumnDescription = "Headers(可以包含如：Authorization授权认证) \n\t 格式：{\"Authorization\":\"userpassword..\"}")]
        public string Headers { get; set; }
        /// <summary>
        /// 请求参数（Post，Put请求用）
        /// {"Authorization":"userpassword.."}
        /// </summary>
        [SugarColumn(ColumnName = "Parameters", IsNullable = true, ColumnDescription = "请求参数（Post，Put请求用）")]
        public string Parameters { get; set; }
        #endregion

        #region 发送邮件相关信息
        /// <summary>
        /// 发送类型
        /// </summary>
        [SugarColumn(ColumnName = "Mail_Msg_Type", IsNullable = true, ColumnDescription = "发送类型")]
        public SendMsgTypeEnum? MailMsgType { get; set; } = SendMsgTypeEnum.None;

        /// <summary>
        /// 邮件接收者
        /// </summary>
        [SugarColumn(ColumnName = "Send_Mailers", IsNullable = true, ColumnDescription = "邮件接收者")]
        public List<string> SendMailers { get; set; }
        #endregion

        #region 发送Hork相关信息
        /// <summary>
        /// 发送类型
        /// </summary>
        [SugarColumn(ColumnName = "Hork_Msg_Type", IsNullable = true, ColumnDescription = "发送类型")]
        public SendMsgTypeEnum? HorkMsgType { get; set; } = SendMsgTypeEnum.None;

        /// <summary>
        /// Hork地址
        /// </summary>
        [SugarColumn(ColumnName = "Hork_Url", IsNullable = true, ColumnDescription = "邮件接收者")]
        public string HorkUrl { get; set; }
        #endregion
        /// <summary>
        /// 执行程序集
        /// </summary>
        [SugarColumn(ColumnName = "Assembly_Name", IsNullable = true, ColumnDescription = "执行程序集")]
        public string AssemblyName { get; set; }

        /// <summary>
        /// 任务所在类
        /// </summary>
        [SugarColumn(ColumnName = "Class_Name", IsNullable = true, ColumnDescription = "任务所在类")]
        public string ClassName { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [SugarColumn(ColumnName = "Description", IsNullable = true, ColumnDescription = "描述")]
        public string Description { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [SugarColumn(ColumnName = "Create_Time", ColumnDescription = "创建时间")]
        public DateTime? CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [SugarColumn(ColumnName = "Modify_Time", IsNullable =true, ColumnDescription = "更新时间")]
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// 是否启动 
        /// </summary>
        [SugarColumn(ColumnName = "Status", ColumnDescription = "是否启动")]
        public StatusEnum? Status { get; set; }
        /// <summary>
        /// 删除标志
        /// </summary>
        [SugarColumn(ColumnName = "Is_Delete", ColumnDescription = "删除标志")]
        public IsDelete? Deleted { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [SugarColumn(ColumnName = "Remark", Length = 4000, IsNullable = true, ColumnDescription = "备注")]
        public string Remark { get; set; }
        #region Ignore Column

        //public long? Creator { get; set; }

        //public long? Modifier { get; set; }

        ///// <summary>
        ///// 任务所在类
        ///// </summary>
        //[SugarColumn(ColumnName = "Job_Class_Name", ColumnDescription = "任务所在类")]
        //public string JobClassName { get; set; }

        ///// <summary>
        ///// 触发器状态 WAITING:等待 PAUSED:暂停ACQUIRED:正常执行 BLOCKED：阻塞 ERROR：错误
        ///// </summary>
        //[SugarColumn(IsIgnore = true, ColumnName = "Trigger_State", ColumnDescription = "触发器状态 WAITING:等待 PAUSED:暂停ACQUIRED:正常执行 BLOCKED：阻塞 ERROR：错误")]
        //public Quartz.TriggerState TriggerState { get; set; }

        ///// <summary>
        ///// 上次执行时间
        ///// </summary>
        //[SugarColumn(IsIgnore = true, ColumnName = "Previous_Fire_Time", ColumnDescription = "上次执行时间")]
        //public DateTime? PreviousFireTime { get; set; }

        ///// <summary>
        ///// 下次执行时间
        ///// </summary>
        //[SugarColumn(IsIgnore = true, ColumnName = "Next_Fire_Time", ColumnDescription = "下次执行时间")]
        //public DateTime? NextFireTime { get; set; }

        ///// <summary>
        ///// 最后一次执行错误
        ///// </summary>
        //[SugarColumn(IsIgnore = true, ColumnName = "Last_Exception", ColumnDescription = "最后一次执行错误")]
        //public string LastException { get; set; } 

        #endregion
    }
}

