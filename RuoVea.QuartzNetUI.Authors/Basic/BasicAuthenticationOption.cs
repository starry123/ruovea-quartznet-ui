﻿using Microsoft.AspNetCore.Authentication;

namespace RuoVea.QuartzNetUI.Authors.Basic
{
    /// <summary>
    /// 
    /// </summary>
    public class BasicAuthenticationOption : AuthenticationSchemeOptions
    {
        /// <summary>
        /// 随机标识
        /// </summary>
        public string Realm { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string UserPwd { get; set; }
    }
}
