﻿echo off
echo "Press B to build images, P to push to registry, any other key to cancel"
set /p op= :
echo "请输入docker小写名称 "
set /p imgname= :
if "%op%"=="B" goto build
if "%op%"=="P" goto push
exit

:build
docker rmi %imgname%/apkimg
docker build -f "Dockerfile" --force-rm -t %imgname%/apkimg .
goto end

:push
docker push %imgname%/apkimg
goto end

:end
pause