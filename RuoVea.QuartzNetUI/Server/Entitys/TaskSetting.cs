﻿using SqlSugar;
namespace RuoVea.QuartzNetUI.Entitys
{
    /// <summary>
    /// 任务调度通知设置
    /// </summary>
    [SugarTable("Qrtz_Task_Setting", tableDescription: "任务调度通知设置")]
    public class TaskSetting
    {
        /// <summary>
        /// 主键
        /// </summary>
        [SugarColumn(ColumnName = "Id", IsPrimaryKey = true, ColumnDescription = "主键")]
        public long Id { get; set; }

        /// <summary>
        /// 代码 
        /// </summary>
        [SugarColumn(ColumnName = "Code", IsNullable = true, ColumnDescription = "分组代码")]
        public string Code { get; set; }
        /// <summary>
        /// 邮箱 
        /// </summary>
        [SugarColumn(ColumnName = "EmailAccount", IsNullable = true,  ColumnDescription = "邮箱")]
        public string EmailAccount { get; set; }

        /// <summary>
        /// 邮箱密码 
        /// </summary>
        [SugarColumn(ColumnName = "EmaillPass", IsNullable = true, ColumnDescription = "邮箱密码")]
        public string EmaillPass { get; set; }

    }
}

