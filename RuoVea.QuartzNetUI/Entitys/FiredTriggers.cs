﻿using SqlSugar;
namespace RuoVea.QuartzNetUI.Entitys
{
    /// <summary>
    /// 存储与已触发的 Trigger 相关的状态信息，以及相联 Job 的执行信息
    ///</summary>
    [SugarTable("Qrtz_Fired_Triggers", tableDescription: "存储与已触发的Trigger相关的状态信息")]
    [SugarIndex("idx_qrtz_ft_trig_inst_name", nameof(FiredTriggers.SchedName), OrderByType.Asc, nameof(FiredTriggers.InstanceName), OrderByType.Asc)]
    [SugarIndex("idx_qrtz_ft_inst_job_req_rcvry", nameof(FiredTriggers.SchedName), OrderByType.Asc, nameof(FiredTriggers.InstanceName), OrderByType.Asc, nameof(FiredTriggers.RequestsRecovery), OrderByType.Asc)]
    [SugarIndex("idx_qrtz_ft_j_g", nameof(FiredTriggers.SchedName), OrderByType.Asc, nameof(FiredTriggers.JobName), OrderByType.Asc, nameof(FiredTriggers.JobGroup), OrderByType.Asc)]
    [SugarIndex("idx_qrtz_ft_jg", nameof(FiredTriggers.SchedName), OrderByType.Asc, nameof(FiredTriggers.JobGroup), OrderByType.Asc)]
    [SugarIndex("idx_qrtz_ft_t_g", nameof(FiredTriggers.SchedName), OrderByType.Asc, nameof(FiredTriggers.TriggerName), OrderByType.Asc, nameof(FiredTriggers.TriggerGroup), OrderByType.Asc)]
    [SugarIndex("idx_qrtz_ft_tg", nameof(FiredTriggers.SchedName), OrderByType.Asc, nameof(FiredTriggers.TriggerGroup), OrderByType.Asc)]
    public class FiredTriggers
    {
        /// <summary>
        /// 调度名称 
        ///</summary>
        [SugarColumn(ColumnName = "Sched_Name", Length = 120, IsPrimaryKey = true, ColumnDescription = "调度名称")]
        public string SchedName { get; set; }
        /// <summary>
        /// 调度器实例id 
        ///</summary>
        [SugarColumn(ColumnName = "Entry_Id", IsPrimaryKey = true, ColumnDescription = "调度器实例id")]
        public string EntryId { get; set; }
        /// <summary>
        /// qrtz_triggers表trigger_name的外键 
        ///</summary>
        [SugarColumn(ColumnName = "Trigger_Name", Length = 150, ColumnDescription = "qrtz_triggers表trigger_name的外键")]
        public string TriggerName { get; set; }
        /// <summary>
        /// qrtz_triggers表trigger_group的外键 
        ///</summary>
        [SugarColumn(ColumnName = "Trigger_Group", Length = 150, ColumnDescription = "qrtz_triggers表trigger_group的外键")]
        public string TriggerGroup { get; set; }
        /// <summary>
        /// 调度器实例名 
        ///</summary>
        [SugarColumn(ColumnName = "Instance_Name", ColumnDescription = "调度器实例名")]
        public string InstanceName { get; set; }
        /// <summary>
        /// 触发的时间 
        ///</summary>
        [SugarColumn(ColumnName = "Fired_Time", ColumnDescription = "触发的时间")]
        public long FiredTime { get; set; }
        /// <summary>
        /// 定时器制定的时间 
        ///</summary>
        [SugarColumn(ColumnName = "Sched_Time", ColumnDescription = "定时器制定的时间")]
        public long SchedTime { get; set; }
        /// <summary>
        /// 优先级 
        ///</summary>
        [SugarColumn(ColumnName = "Priority", ColumnDescription = "优先级")]
        public int Priority { get; set; }
        /// <summary>
        /// 状态 
        ///</summary>
        [SugarColumn(ColumnName = "State",Length=16, ColumnDescription = "State")]
        public string State { get; set; }
        /// <summary>
        /// 集群中job的名字 
        ///</summary>
        [SugarColumn(ColumnName = "Job_Name", IsNullable = true, DefaultValue = null, ColumnDescription = "集群中job的名字")]
        public string JobName { get; set; }
        /// <summary>
        /// 集群中job的所属组的名字 
        ///</summary>
        [SugarColumn(ColumnName = "Job_Group", IsNullable = true, DefaultValue = null, ColumnDescription = "集群中job的所属组的名字")]
        public string JobGroup { get; set; }
        /// <summary>
        /// 是否并发 
        ///</summary>
        [SugarColumn(ColumnName = "Is_Nonconcurrent", IsNullable = true, DefaultValue = null, ColumnDescription = "是否并发")]
        public byte? IsNonconcurrent { get; set; }
        /// <summary>
        /// 是否接受恢复执行，默认为false，设置了RequestsRecovery为true，则会被重新执行 
        ///</summary>
        [SugarColumn(ColumnName = "Requests_Recovery", IsNullable = true, DefaultValue = null, ColumnDescription = "是否接受恢复执行，默认为false，设置了RequestsRecovery为true，则会被重新执行 ")]
        public byte? RequestsRecovery { get; set; }
    }
}
