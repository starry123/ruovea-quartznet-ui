﻿using RuoVea.ExDto;
using RuoVea.QuartzNetUI.Entitys;
using RuoVea.QuartzNetUI.Server.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RuoVea.QuartzNetUI.Server
{
    /// <summary>
    /// 任务日志接口
    /// </summary>
    public interface ITaskLogService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tasksLog"></param>
        /// <returns></returns>
        Task<RestfulResult> InsertAsync(TaskLog tasksLog);
        /// <summary>
        /// 获取任务日志
        /// </summary>
        /// <param name="data"></param>
        /// <returns> </returns>
        Task<RestfulResult<IList<TaskLog>>> GetTaskLogListAsync(TaskLogQueryDto data);
        /// <summary>
        /// 获取任务日志
        /// </summary>
        /// <param name="data"></param>
        /// <returns> </returns>
        Task<RestfulResult<PageResult<TaskLog>>> GetTaskLogPageListAsync(TaskLogPageDto data);
        /// <summary>
        /// 根据Id列表或者时间区间删除日志
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<RestfulResult> DeleteAsync(TaskLogDeleteDto data);
    }
}
