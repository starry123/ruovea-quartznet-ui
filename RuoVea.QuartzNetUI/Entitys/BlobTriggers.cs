﻿using SqlSugar;
namespace RuoVea.QuartzNetUI.Entitys
{
    ////普通索引
    //[SugarIndex("index_codetable1_name", nameof(CodeFirstTable1.Name), OrderByType.Asc)]
    ////唯一索引 (true表示唯一索引 或者叫 唯一约束)
    //[SugarIndex("unique_codetable1_CreateTime", nameof(CodeFirstTable1.CreateTime), OrderByType.Desc, true)]
    ////复合普通索引
    //[SugarIndex("index_codetable1_nameid", nameof(CodeFirstTable1.Name), OrderByType.Asc,nameof(CodeFirstTable1.Id), OrderByType.Desc)]

    /// <summary>
    /// 作为 Blob 类型存储(用于 Quartz 用户用 JDBC 创建他们自己定制的 Trigger 类型，JobStore 并不知道如何存储实例的时候)
    ///</summary>
    [SugarTable("Qrtz_Blob_Triggers", tableDescription: "作为Blob类型存储")]
    public class BlobTriggers
    {
        /// <summary>
        /// 调度名称 
        ///</summary>
        [SugarColumn(ColumnName = "Sched_Name",Length =120, IsPrimaryKey = true, ColumnDescription = "调度名称")]
        public string SchedName { get; set; }
        /// <summary>
        /// qrtz_triggers表trigger_name的外键 
        ///</summary>
        [SugarColumn(ColumnName = "Trigger_Name", Length = 150, IsPrimaryKey = true, ColumnDescription = "qrtz_triggers表trigger_name的外键")]
        public string TriggerName { get; set; }
        /// <summary>
        /// qrtz_triggers表trigger_group的外键 
        ///</summary>
        [SugarColumn(ColumnName = "Trigger_Group", Length = 150, IsPrimaryKey = true, ColumnDescription = "qrtz_triggers表trigger_group的外键")]
        public string TriggerGroup { get; set; }
        /// <summary>
        /// 一个blob字段，存放持久化Trigger对象 
        ///</summary>
        [SugarColumn(ColumnName = "Blob_Data",IsNullable =true,DefaultValue =null, ColumnDescription = "一个blob字段，存放持久化Trigger对象 ")]
        public byte[] BlobData { get; set; }
    }
}
