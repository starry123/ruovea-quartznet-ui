using RuoVea.QuartzNetUI.Authors;

var builder = WebApplication.CreateBuilder(args);

//Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddQuartzNetAuthorServer(builder.Configuration.GetSection("QuartzNetUI"));
//builder.Services.AddQuartzNetAuthorServer(op=> {

//    op.AuthorUrl = "job";op.RootUrl = "job";op.UserName = "admins";
//    op.UserPwd = "admins";op. DbType = RuoVea.QuartzNetUI.DbTypeEnum.SQLite;op. ConnectionString = "Data Source=./ruovea2.db" ; });

//builder.Services.AddHttpClientFactory();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseQuartzAuthorUI();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
 