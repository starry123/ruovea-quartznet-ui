﻿namespace RuoVea.QuartzNetUI.Server.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class TaskLogDeleteDto
    {
        /// <summary>
        /// 日志主键列表
        /// </summary>
        public IList<long> Ids { get; set; }
        /// <summary>
        /// @I18n.StartTime
        /// </summary>

        public DateTime? StartTime { get; set; }
        /// <summary>
        /// 结束时间
        /// </summary>

        public DateTime? EndTime { get; set; }
    }
}

