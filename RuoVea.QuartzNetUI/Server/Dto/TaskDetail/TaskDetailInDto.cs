﻿using RuoVea.ExEnum;
using RuoVea.QuartzNetUI.Server.Enums;

namespace RuoVea.QuartzNetUI.Server.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class TaskDetailInDto {
        /// <summary>
        /// 主键
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SchedName { get; set; }
        /// <summary>
        /// 任务名称
        /// </summary>
        public string JobName { get; set; }

        /// <summary>
        /// 任务分组
        /// </summary>
        public string JobGroup { get; set; }

        /// <summary>
        /// @I18n.StartTime
        /// </summary>
        public DateTime? BeginTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime? EndTime { get; set; } = DateTime.MaxValue;

        /// <summary>
        /// Cron表达式
        /// </summary>
        public string Cron { get; set; }

        /// <summary>
        /// 执行次数（默认无限循环）
        /// </summary>
        public int? RunTimes { get; set; }

        /// <summary>
        /// 执行间隔时间，单位秒（如果有Cron，则IntervalSecond失效）
        /// </summary>
        public int? IntervalSecond { get; set; }

        /// <summary>
        /// 调度方式
        /// </summary>
        public ScheduleTypeEnum? ScheduleType { get; set; }
        /// <summary>
        /// 触发器类型
        /// </summary>
        public TriggerTypeEnum? TriggerType { get; set; } = TriggerTypeEnum.Simple;
        /// <summary>
        /// 运行类型
        /// </summary>
        public RequestTypeEnum? RunType { get; set; } = RequestTypeEnum.Get;
        ///// <summary>
        ///// 触发器状态 WAITING:等待 PAUSED:暂停ACQUIRED:正常执行 BLOCKED：阻塞 ERROR：错误
        ///// </summary>
        //public TriggerState TriggerState { get; set; }
        ///// <summary>
        ///// 时间间隔
        ///// </summary>
        //public string Interval { get; set; }

        /// <summary>
        /// 请求url
        /// </summary>
        public string RequestUrl { get; set; }

        /// <summary>
        /// Headers(可以包含如：Authorization授权认证)
        /// 格式：{"Authorization":"userpassword.."}
        /// </summary>
        public string Headers { get; set; }
        /// <summary>
        /// 请求参数（Post，Put请求用）
        /// {"Authorization":"userpassword.."}
        /// </summary>
        public string Parameters { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 发送类型
        /// </summary>
        public SendMsgTypeEnum? MailMsgType { get; set; } = SendMsgTypeEnum.None;
        /// <summary>
        /// 邮件接收者
        /// </summary>
        public List<string> SendMailers { get; set; }

        #region 发送Hork相关信息
        /// <summary>
        /// 发送类型
        /// </summary>
        public SendMsgTypeEnum? HorkMsgType { get; set; } = SendMsgTypeEnum.None;

        /// <summary>
        /// Hork地址
        /// </summary>
        public string HorkUrl { get; set; }
        #endregion
        /// <summary>
        /// 
        /// </summary>
        public string AssemblyName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 是否启动 
        /// </summary> 
        public StatusEnum? Status { get; set; }

    }
}
