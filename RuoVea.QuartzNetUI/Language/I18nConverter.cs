﻿using System.Collections;
using System.Globalization;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;

namespace RuoVea.QuartzNetUI.Language
{
    /// <summary>
    /// 
    /// </summary>
    public class I18nConverter : I18n
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string ConvertResxToJson()
        {
            var resourceSet = ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            // 读取资源并存储到字典
            var resourceDictionary = new Dictionary<string, string>();
            foreach (DictionaryEntry entry in resourceSet)
                resourceDictionary[(string)entry.Key] = (string)entry.Value;

            // 序列化为JSON
            var json = JsonSerializer.Serialize(resourceDictionary, new JsonSerializerOptions { WriteIndented = true,Encoder = JavaScriptEncoder.Create(UnicodeRanges.All) });
            return json             ;
        }
    }
}
