﻿using SqlSugar;
namespace RuoVea.QuartzNetUI.Entitys
{
    /// <summary>
    /// 保存触发器的基本信息
    ///</summary>
    [SugarTable("Qrtz_Triggers", tableDescription: "保存触发器的基本信息")]
    [SugarIndex("idx_qrtz_t_j", nameof(Triggers.SchedName), OrderByType.Asc, nameof(Triggers.JobName), OrderByType.Asc, nameof(Triggers.JobGroup), OrderByType.Asc)]
    [SugarIndex("idx_qrtz_t_jg", nameof(Triggers.SchedName), OrderByType.Asc, nameof(Triggers.JobGroup), OrderByType.Asc)]
    [SugarIndex("idx_qrtz_t_c", nameof(Triggers.SchedName), OrderByType.Asc, nameof(Triggers.CalendarName), OrderByType.Asc)]
    [SugarIndex("idx_qrtz_t_g", nameof(Triggers.SchedName), OrderByType.Asc, nameof(Triggers.TriggerGroup), OrderByType.Asc)]
    [SugarIndex("idx_qrtz_t_state", nameof(Triggers.SchedName), OrderByType.Asc, nameof(Triggers.TriggerState), OrderByType.Asc)]
    [SugarIndex("idx_qrtz_t_n_state", nameof(Triggers.SchedName), OrderByType.Asc, nameof(Triggers.TriggerName), OrderByType.Asc, nameof(Triggers.TriggerGroup), OrderByType.Asc, nameof(Triggers.TriggerState), OrderByType.Asc)]
    [SugarIndex("idx_qrtz_t_n_g_state", nameof(Triggers.SchedName), OrderByType.Asc, nameof(Triggers.TriggerGroup), OrderByType.Asc, nameof(Triggers.TriggerState), OrderByType.Asc)]
    [SugarIndex("idx_qrtz_t_next_fire_time", nameof(Triggers.SchedName), OrderByType.Asc, nameof(Triggers.NextFireTime), OrderByType.Asc)]
    [SugarIndex("idx_qrtz_t_nft_st", nameof(Triggers.SchedName), OrderByType.Asc, nameof(Triggers.TriggerState), OrderByType.Asc, nameof(Triggers.NextFireTime), OrderByType.Asc)]
    [SugarIndex("idx_qrtz_t_nft_misfire", nameof(Triggers.SchedName), OrderByType.Asc, nameof(Triggers.MisfireInstr), OrderByType.Asc, nameof(Triggers.NextFireTime), OrderByType.Asc)]
    [SugarIndex("idx_qrtz_t_nft_st_misfire", nameof(Triggers.SchedName), OrderByType.Asc, nameof(Triggers.MisfireInstr), OrderByType.Asc, nameof(Triggers.NextFireTime), OrderByType.Asc, nameof(Triggers.TriggerState), OrderByType.Asc)]
    [SugarIndex("idx_qrtz_t_nft_st_misfire_grp", nameof(Triggers.SchedName), OrderByType.Asc, nameof(Triggers.MisfireInstr), OrderByType.Asc, nameof(Triggers.NextFireTime), OrderByType.Asc, nameof(Triggers.TriggerGroup), OrderByType.Asc, nameof(Triggers.TriggerState), OrderByType.Asc)]
    public class Triggers
    {
        /// <summary>
        /// 调度名称 
        ///</summary>
        [SugarColumn(ColumnName = "Sched_Name", Length = 120, IsPrimaryKey = true, ColumnDescription = "调度名称")]
        public string SchedName { get; set; }
        /// <summary>
        /// 触发器的名字 
        ///</summary>
        [SugarColumn(ColumnName = "Trigger_Name", Length = 150, IsPrimaryKey = true, ColumnDescription = "触发器的名字")]
        public string TriggerName { get; set; }
        /// <summary>
        /// 触发器所属组的名字 
        ///</summary>
        [SugarColumn(ColumnName = "Trigger_Group", Length = 150, IsPrimaryKey = true, ColumnDescription = "触发器所属组的名字")]
        public string TriggerGroup { get; set; }
        /// <summary>
        /// qrtz_job_details表Job_Name的外键 
        ///</summary>
        [SugarColumn(ColumnName = "Job_Name", Length = 150, ColumnDescription = "qrtz_job_details表Job_Name的外键")]
        public string JobName { get; set; }
        /// <summary>
        /// qrtz_job_details表Job_Group的外键 
        ///</summary>
        [SugarColumn(ColumnName = "Job_Group", Length = 150, ColumnDescription = "qrtz_job_details表Job_Group的外键")]
        public string JobGroup { get; set; }
        /// <summary>
        /// 详细描述信息 
        ///</summary>
        [SugarColumn(ColumnName = "Description", Length = 250, ColumnDescription = "详细描述信息")]
        public string Description { get; set; }
        /// <summary>
        /// 上一次触发时间（毫秒） 
        ///</summary>
        [SugarColumn(ColumnName = "Next_Fire_Time", ColumnDescription = "上一次触发时间（毫秒） ")]
        public long? NextFireTime { get; set; }
        /// <summary>
        /// 下一次触发时间，默认为-1，意味不会自动触发 
        ///</summary>
        [SugarColumn(ColumnName = "Prev_Fire_Time", ColumnDescription = "下一次触发时间，默认为-1，意味不会自动触发")]
        public long? PrevFireTime { get; set; }
        /// <summary>
        /// 优先级 
        ///</summary>
        [SugarColumn(ColumnName = "Priority", ColumnDescription = "优先级")]
        public int? Priority { get; set; }
        /// <summary>
        /// 当前触发器状态，设置为ACQUIRED,如果设置为WAITING,则job不会触发 （ WAITING:等待 PAUSED:暂停ACQUIRED:正常执行 BLOCKED：阻塞 ERROR：错误） 
        ///</summary>
        [SugarColumn(ColumnName = "Trigger_State", Length = 16, ColumnDescription = "当前触发器状态，设置为ACQUIRED,如果设置为WAITING,则job不会触发 （ WAITING:等待 PAUSED:暂停ACQUIRED:正常执行 BLOCKED：阻塞 ERROR：错误）")]
        public string TriggerState { get; set; }
        /// <summary>
        /// 触发器的类型，使用cron表达式 
        ///</summary>
        [SugarColumn(ColumnName = "Trigger_Type", Length = 8, ColumnDescription = "触发器的类型，使用cron表达式 ")]
        public string TriggerType { get; set; }
        /// <summary>
        /// @I18n.StartTime 
        ///</summary>
        [SugarColumn(ColumnName = "Start_Time", ColumnDescription = "@I18n.StartTime")]
        public long StartTime { get; set; }
        /// <summary>
        /// 结束时间 
        ///</summary>
        [SugarColumn(ColumnName = "End_Time", ColumnDescription = "结束时间")]
        public long? EndTime { get; set; }
        /// <summary>
        /// 日程表名称，表qrtz_calendars的calendar_name字段外键 
        ///</summary>
        [SugarColumn(ColumnName = "Calendar_Name", Length = 200, ColumnDescription = "日程表名称，表qrtz_calendars的calendar_name字段外键")]
        public string CalendarName { get; set; }
        /// <summary>
        /// 措施或者是补偿执行的策略 
        ///</summary>
        [SugarColumn(ColumnName = "Misfire_Instr", ColumnDescription = "措施或者是补偿执行的策略")]
        public short? MisfireInstr { get; set; }
        /// <summary>
        /// 一个blob字段，存放持久化job对象 
        ///</summary>
        [SugarColumn(ColumnName = "Job_Data", ColumnDescription = "一个blob字段，存放持久化job对象")]
        public byte[] JobData { get; set; }
    }
}
