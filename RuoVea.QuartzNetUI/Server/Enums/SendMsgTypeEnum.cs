﻿namespace RuoVea.QuartzNetUI.Server.Enums
{
    /// <summary>
    /// 
    /// </summary>
    public enum SendMsgTypeEnum
    {
        /// <summary>
        /// 都不发生
        /// </summary>
        None = 0,
        /// <summary>
        /// 异常时发送
        /// </summary>
        Err = 1,
        /// <summary>
        /// 成功时发送
        /// </summary>
        OK = 2,
        /// <summary>
        /// 全部发送
        /// </summary>
        All = 3
    }
}

