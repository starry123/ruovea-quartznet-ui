﻿using SqlSugar;
namespace RuoVea.QuartzNetUI.Entitys
{
    /// <summary>
    /// 存储每一个已配置的
    ///</summary>
    [SugarTable("Qrtz_Job_Details", tableDescription: "存储每一个已配置的")]
    [SugarIndex("idx_qrtz_j_req_recovery", nameof(JobDetails.SchedName), OrderByType.Asc, nameof(JobDetails.RequestsRecovery), OrderByType.Asc)]
    [SugarIndex("idx_qrtz_j_grp", nameof(JobDetails.SchedName), OrderByType.Asc, nameof(JobDetails.JobGroup), OrderByType.Asc)]
    public class JobDetails
    {
        /// <summary>
        /// 调度名称 
        ///</summary>
        [SugarColumn(ColumnName = "Sched_Name", Length = 120, IsPrimaryKey = true, ColumnDescription = "调度名称")]
        public string SchedName { get; set; }
        /// <summary>
        /// 集群中job的名字 
        ///</summary>
        [SugarColumn(ColumnName = "Job_Name", Length = 150, IsPrimaryKey = true, ColumnDescription = "集群中job的名字")]
        public string JobName { get; set; }
        /// <summary>
        /// 集群中job的所属组的名字 
        ///</summary>
        [SugarColumn(ColumnName = "Job_Group", Length = 150, IsPrimaryKey = true, ColumnDescription = "集群中job的所属组的名字")]
        public string JobGroup { get; set; }
        /// <summary>
        /// 详细描述信息 
        ///</summary>
        [SugarColumn(ColumnName = "Description",Length=250 , IsNullable = true, DefaultValue = null, ColumnDescription = "详细描述信息")]
        public string Description { get; set; }
        /// <summary>
        /// 集群中个notejob实现类的全限定名,quartz就是根据这个路径到classpath找到该job类 
        ///</summary>
        [SugarColumn(ColumnName = "Job_Class_Name",Length=250, ColumnDescription = "集群中个notejob实现类的全限定名,quartz就是根据这个路径到classpath找到该job类")]
        public string JobClassName { get; set; }
        /// <summary>
        /// 是否持久化,把该属性设置为1，quartz会把job持久化到数据库中 
        ///</summary>
        [SugarColumn(ColumnName = "Is_Durable", ColumnDescription = "是否持久化,把该属性设置为1，quartz会把job持久化到数据库中 ")]
        public byte IsDurable { get; set; }
        /// <summary>
        /// 是否并发执行 
        ///</summary>
        [SugarColumn(ColumnName = "Is_Nonconcurrent", ColumnDescription = "是否并发执行")]
        public byte IsNonconcurrent { get; set; }
        /// <summary>
        /// 是否更新数据 
        ///</summary>
        [SugarColumn(ColumnName = "Is_Update_Data", ColumnDescription = "是否更新数据")]
        public byte IsUpdateData { get; set; }
        /// <summary>
        /// 是否接受恢复执行，默认为false，设置了RequestsRecovery为true，则该job会被重新执行 
        ///</summary>
        [SugarColumn(ColumnName = "Requests_Recovery", ColumnDescription = "是否接受恢复执行，默认为false，设置了RequestsRecovery为true，则该job会被重新执行")]
        public byte RequestsRecovery { get; set; }
        /// <summary>
        /// 一个blob字段，存放持久化job对象 
        ///</summary>
        [SugarColumn(ColumnName = "Job_Data", IsNullable = true, DefaultValue = null, ColumnDescription = "存放持久化job对象", ColumnDataType = "blob")]
        public byte[] JobData { get; set; }
    }
}
