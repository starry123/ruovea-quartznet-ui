# QuartzUI、 Quartz.HttpJob

#### 介绍

QuartzNet UI界面

简单任务调度平台（RuoVea.QuartzNetUI）：基于.Net5.0 、.Net6.0 、.Net7.0、.Net8.0、  Quartz.AspNetCore 3.7.0 构建的简单、跨平台的任务调度系统。
系统业务简单、代码清晰，如果您只是单纯只需简单任务执行、监控、提醒这套系统就非常适合。没有多余的功能，简单扩展下基本可以满足日常需求。
 支持的数据库有 MySql、SqlServer、PostgreSQL、SQLite、Oracle、Firebird(后续支持)

#### 数据库表创建-执行脚本地址

https://gitee.com/starry123/ruovea-quartznet-ui/tree/develop/Doc/Sql


#### 安装教程

1. Install-Package RuoVea.QuartzNetUI

2.  builder.Services.AddQuartzNetServer(builder.Configuration.GetSection("ConnectionStrings"));

3.  app.UseQuartzUI();

#### 使用说明

1、添加新建项目
![添加新建项目](Doc/img/setp/1.png)
项目名
![取名](Doc/img/setp/2.png)
2、点击Nuget管理
![Nuget管理](Doc/img/setp/3.png)
添加 RuoVea.QuartzNetUI 包
![添加Nuget包](Doc/img/setp/4.png)
3、添加代码引用
![添加代码引用](Doc/img/setp/5.png)
4、配置链接字符串
![添加链接字符串](Doc/img/setp/6.png)

5、添加对应数据库的驱动  

6、创建对应的数据库名

7、找到对应数据库脚本执行建表

8、运行
![运行首页](Doc/img/setp/8.png)

9、输入地址  /job
![任务首页](Doc/img/setp/9.png)

SQLite 引用   System.Data.SQLite" Version="1.0.118"

MySql 引用  MySql.Data

有问题可详细咨询

#### 配置文件增加

```
  /* 链接字符串参考地址 https://www.donet5.com/Home/Doc?typeId=1220 */
  "ConnectionStrings": {
    "DbType": "SQLite", //MySql、SqlServer、PostgreSQL、SQLite、Oracle、Firebird(后续支持)
    "ConnectionString": "Data Source=./quartznetui.db"

    //"DbType": "MySql", //MySql、SqlServer、PostgreSQL、SQLite、Oracle、Firebird(后续支持)
    //"ConnectionString": "server=localhost;Database=quartznetui;Uid=root;Pwd=haosql",

    //"DbType": "SqlServer", //MySql、SqlServer、PostgreSQL、SQLite、Oracle、Firebird(后续支持)
    //"ConnectionString": "server=.;uid=sa;pwd=haosql;database=quartznetui",

    //"DbType": "PostgreSQL", //MySql、SqlServer、PostgreSQL、SQLite、Oracle、Firebird(后续支持)
    //"ConnectionString": "PORT=5432;DATABASE=quartznetui;HOST=localhost;PASSWORD=haosql;USER ID=postgres",

    //"DbType": "Oracle", //MySql、SqlServer、PostgreSQL、SQLite、Oracle、Firebird(后续支持)
    //"ConnectionString": "Data Source=localhost/quartznetui;User ID=system;Password=haha"

  }

```
### 界面截图
周期作业

![周期作业](Doc/img/dashboard.png)

周期作业编辑

![周期作业编辑](Doc/img/edit.png)
![周期作业编辑](Doc/img/edit2.png)
![周期作业编辑](Doc/img/edit3.png)
![周期作业编辑](Doc/img/edit4.png)

执行日志

![执行日志](Doc/img/log.png)

作业分析

![作业分析](Doc/img/period.png)

设置

![设置](Doc/img/setting.png)

Info

![Info](Doc/img/guide.png)

