﻿using SqlSugar;
namespace RuoVea.QuartzNetUI.Entitys
{
    /// <summary>
    /// 存储触发器的cron表达式表
    ///</summary>
    [SugarTable("Qrtz_Cron_Triggers", tableDescription: "存储触发器的cron表达式表")]
    public class CronTriggers
    {
        /// <summary>
        /// 调度名称 
        ///</summary>
        [SugarColumn(ColumnName = "Sched_Name", Length = 120, IsPrimaryKey = true, ColumnDescription = "调度名称")]
        public string SchedName { get; set; }
        /// <summary>
        /// qrtz_triggers表trigger_name的外键 
        ///</summary>
        [SugarColumn(ColumnName = "Trigger_Name", Length = 150, IsPrimaryKey = true, ColumnDescription = "qrtz_triggers表trigger_name的外键")]
        public string TriggerName { get; set; }
        /// <summary>
        /// qrtz_triggers表trigger_group的外键 
        ///</summary>
        [SugarColumn(ColumnName = "Trigger_Group", Length = 150, IsPrimaryKey = true, ColumnDescription = "qrtz_triggers表trigger_group的外键")]
        public string TriggerGroup { get; set; }
        /// <summary>
        /// cron表达式 
        ///</summary>
        [SugarColumn(ColumnName = "Cron_Expression", Length = 250, ColumnDescription = "cron表达式")]
        public string CronExpression { get; set; }
        /// <summary>
        /// 时区 
        ///</summary>
        [SugarColumn(ColumnName = "Time_Zone_Id", IsNullable = true, DefaultValue = null, Length = 80, ColumnDescription = "时区")]
        public string TimeZoneId { get; set; }
    }
}
