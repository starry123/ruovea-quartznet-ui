﻿using SqlSugar;
namespace RuoVea.QuartzNetUI.Entitys
{
    /// <summary>
    /// 存储CalendarIntervalTrigger和DailyTimeIntervalTrigger
    ///</summary>
    [SugarTable("Qrtz_Simprop_Triggers", tableDescription: "存储CalendarIntervalTrigger和DailyTimeIntervalTrigger")]
    public class SimpropTriggers
    {
        /// <summary>
        /// 调度名称 
        ///</summary>
        [SugarColumn(ColumnName = "Sched_Name", Length = 120, IsPrimaryKey = true, ColumnDescription = "调度名称")]
        public string SchedName { get; set; }
        /// <summary>
        /// qrtz_triggers表trigger_ name的外键 
        ///</summary>
        [SugarColumn(ColumnName = "Trigger_Name", Length = 150, IsPrimaryKey = true, ColumnDescription = "qrtz_triggers表trigger_ name的外键 ")]
        public string TriggerName { get; set; }
        /// <summary>
        /// qrtz_triggers表trigger_group的外键 
        ///</summary>
        [SugarColumn(ColumnName = "Trigger_Group", Length = 150, IsPrimaryKey = true, ColumnDescription = "qrtz_triggers表trigger_group的外键")]
        public string TriggerGroup { get; set; }
        /// <summary>
        /// string类型的trigger的第一个参数 
        ///</summary>
        [SugarColumn(ColumnName = "Str_Prop_1", Length = 512, IsNullable = true, DefaultValue = null, ColumnDescription = "string类型的trigger的第一个参数 ")]
        public string StrProp1 { get; set; }
        /// <summary>
        /// string类型的trigger的第二个参数 
        ///</summary>
        [SugarColumn(ColumnName = "Str_Prop_2", Length = 512, IsNullable = true, DefaultValue = null, ColumnDescription = " string类型的trigger的第二个参数")]
        public string StrProp2 { get; set; }
        /// <summary>
        /// string类型的trigger的第三个参数 
        ///</summary>
        [SugarColumn(ColumnName = "Str_Prop_3", Length = 512, IsNullable = true, DefaultValue = null, ColumnDescription = "string类型的trigger的第三个参数")]
        public string StrProp3 { get; set; }
        /// <summary>
        /// int类型的trigger的第一个参数 
        ///</summary>
        [SugarColumn(ColumnName = "Int_Prop_1", IsNullable = true, DefaultValue = null, ColumnDescription = "int类型的trigger的第一个参数")]
        public int? IntProp1 { get; set; }
        /// <summary>
        /// int类型的trigger的第二个参数 
        ///</summary>
        [SugarColumn(ColumnName = "Int_Prop_2", IsNullable = true, DefaultValue = null, ColumnDescription = "int类型的trigger的第二个参数")]
        public int? IntProp2 { get; set; }
        /// <summary>
        /// long类型的trigger的第一个参数 
        ///</summary>
        [SugarColumn(ColumnName = "Long_Prop_1", IsNullable = true, DefaultValue = null, ColumnDescription = "long类型的trigger的第一个参数")]
        public long? LongProp1 { get; set; }
        /// <summary>
        /// long类型的trigger的第二个参数 
        ///</summary>
        [SugarColumn(ColumnName = "Long_Prop_2", IsNullable = true, DefaultValue = null, ColumnDescription = "long类型的trigger的第二个参数")]
        public long? LongProp2 { get; set; }
        /// <summary>
        /// decimal类型的trigger的第一个参数 
        ///</summary>
        [SugarColumn(ColumnName = "Dec_Prop_1", Length = 9, IsNullable = true, DefaultValue = null, ColumnDescription = "decimal类型的trigger的第一个参数")]
        public decimal? DecProp1 { get; set; }
        /// <summary>
        /// decimal类型的trigger的第二个参数 
        ///</summary>
        [SugarColumn(ColumnName = "Dec_Prop_2", Length = 9, IsNullable = true, DefaultValue = null, ColumnDescription = "decimal类型的trigger的第二个参数")]
        public decimal? DecProp2 { get; set; }
        /// <summary>
        /// Boolean类型的trigger的第一个参数 
        ///</summary>
        [SugarColumn(ColumnName = "Bool_Prop_1", IsNullable = true, DefaultValue = null, ColumnDescription = "Boolean类型的trigger的第一个参数")]
        public byte? BoolProp1 { get; set; }
        /// <summary>
        /// Boolean类型的trigger的第二个参数 
        ///</summary>
        [SugarColumn(ColumnName = "Bool_Prop_2", IsNullable = true, DefaultValue = null, ColumnDescription = "Boolean类型的trigger的第二个参数")]
        public byte? BoolProp2 { get; set; }
        /// <summary>
        ///  
        ///</summary>
        [SugarColumn(ColumnName = "Time_Zone_Id", IsNullable = true, DefaultValue = null, Length = 80, ColumnDescription = "时区")]
        public string TimeZoneId { get; set; }
    }
}
