﻿namespace RuoVea.QuartzNetUI
{
    /// <summary>
    /// 配置信息
    /// </summary>
    public class QuartzNetUIOption
    {
        /// <summary>
        /// 
        /// </summary>
        public string GroupName { get; set; } 

        /// <summary>
        /// MySql、SqlServer、PostgreSQL、SQLite、Oracle、Firebird
        /// </summary>
        public DbTypeEnum DbType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ConnectionString { get; set; }
        /// <summary>
        /// 任务路径
        /// </summary>
        public string RootUrl { get; set; }

        /// <summary>
        /// 开启集群
        /// </summary>
        public bool IsClustered { get; set; } = false;
        /// <summary>
        /// 任务编码
        /// </summary>
        public string SchedulerId { get; set; }
        ///// <summary>
        ///// 任务名称
        ///// </summary>
        //public string SchedulerName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool ShowMenu { get; set; } = true;

        /// <summary>
        /// logo地址
        /// </summary>
        public string LogoUrl { get; set; }

        /// <summary>
        /// Ui标题 
        /// </summary>
        public string UiTitle { get; set; }
    }

    /// <summary>
    /// 数据库类型
    /// </summary>
    public enum DbTypeEnum {
        /// <summary>
        /// MySql
        /// </summary>
        MySql,
        /// <summary>
        /// SqlServer
        /// </summary>
        SqlServer,
        /// <summary>
        /// PostgreSQL
        /// </summary>
        PostgreSQL,
        /// <summary>
        /// SQLite
        /// </summary>
        SQLite,
        /// <summary>
        /// Oracle
        /// </summary>
        Oracle,
        /// <summary>
        /// Firebird
        /// </summary>
        Firebird
    }
}
