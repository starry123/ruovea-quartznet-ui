﻿using SqlSugar;
namespace RuoVea.QuartzNetUI.Entitys
{
    /// <summary>
    /// 存储程序的悲观锁的信息(假如使用了悲观锁)
    ///</summary>
    [SugarTable("Qrtz_Locks", tableDescription: "存储程序的悲观锁的信息")]
    public class Locks
    {
        /// <summary>
        /// 调度名称 
        ///</summary>
        [SugarColumn(ColumnName = "Sched_Name", Length = 120, IsPrimaryKey = true, ColumnDescription = "调度名称")]
        public string SchedName { get; set; }
        /// <summary>
        /// 悲观锁名称 
        ///</summary>
        [SugarColumn(ColumnName = "Lock_Name", Length = 40, IsPrimaryKey = true, ColumnDescription = "悲观锁名称")]
        public string LockName { get; set; }
    }
}
