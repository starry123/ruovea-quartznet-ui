﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using RuoVea.QuartzNetUI.Authors.Language;

namespace RuoVea.QuartzNetUI.Authors.Basic
{
    /// <summary>
    /// 
    /// </summary>
    public class BasicAuthenticationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="next"></param>
        /// <param name="LoggerFactory"></param>
        public BasicAuthenticationMiddleware(RequestDelegate next, ILoggerFactory LoggerFactory)
        {
            _next = next;
            _logger = LoggerFactory.CreateLogger<BasicAuthenticationMiddleware>();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="authenticationService"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext httpContext, IAuthenticationService authenticationService)
        {
            var authenticated = await authenticationService.AuthenticateAsync(httpContext, BasicAuthenticationScheme.DefaultScheme);
            _logger.LogInformation(string.Format(I18n.AccessStatus, authenticated.Succeeded));
            if (!authenticated.Succeeded)
            {
                await authenticationService.ChallengeAsync(httpContext, BasicAuthenticationScheme.DefaultScheme, new AuthenticationProperties { });
                return;
            }
            await _next(httpContext);
        }
    }
}
