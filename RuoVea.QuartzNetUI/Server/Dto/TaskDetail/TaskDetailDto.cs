﻿using Quartz;
using RuoVea.ExEnum;
using RuoVea.ExUtil;
using RuoVea.QuartzNetUI.Server.Enums;

namespace RuoVea.QuartzNetUI.Server.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class TaskDetailDto
    {
        /// <summary>
        /// 主键
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SchedName { get; set; }
        /// <summary>
        /// 任务名称
        /// </summary>
        public string JobName { get; set; }

        /// <summary>
        /// 任务分组
        /// </summary>
        public string JobGroup { get; set; }

        /// <summary>
        /// @I18n.StartTime
        /// </summary>
        public DateTime? BeginTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime? EndTime { get; set; } = DateTime.MaxValue;

        /// <summary>
        /// Cron表达式
        /// </summary>
        public string Cron { get; set; }

        /// <summary>
        /// 执行次数（默认无限循环）
        /// </summary>
        public int? RunTimes { get; set; }

        /// <summary>
        /// 执行间隔时间，单位秒（如果有Cron，则IntervalSecond失效）
        /// </summary>
        public int? IntervalSecond { get; set; }

        /// <summary>
        /// 调度方式
        /// </summary>
        public ScheduleTypeEnum? ScheduleType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ScheduleTypeText
        {
            get
            {
                if (ScheduleType == ScheduleTypeEnum.Http) return "Http";
                else if (ScheduleType == ScheduleTypeEnum.GRPC) return "GRPC";
                return "";
            }
        }

        /// <summary>
        /// 触发器类型
        /// </summary>
        public TriggerTypeEnum? TriggerType { get; set; } = TriggerTypeEnum.Simple;
        /// <summary>
        /// 
        /// </summary>
        public string TriggerTypeText
        {
            get
            {
                if (TriggerType == TriggerTypeEnum.Simple) return "Simple";
                else if (TriggerType == TriggerTypeEnum.Corn) return "Corn";
                return "";
            }
        }

        /// <summary>
        /// 运行类型
        /// </summary>
        public RequestTypeEnum? RunType { get; set; } = RequestTypeEnum.Get;
        /// <summary>
        /// 
        /// </summary>
        public string RunTypeText
        {
            get
            {
                if (RunType == RequestTypeEnum.Run) return "Run";
                else if (RunType == RequestTypeEnum.Get) return "Get";
                else if (RunType == RequestTypeEnum.Post) return "Post";
                else if (RunType == RequestTypeEnum.Put) return "Put";
                else if (RunType == RequestTypeEnum.Delete) return "Delete";
                else return "";
            }
        }

        /// <summary>
        /// 触发器状态 WAITING:等待 PAUSED:暂停ACQUIRED:正常执行 BLOCKED：阻塞 ERROR：错误
        /// </summary>
        public TriggerState TriggerState { get; set; }

        /// <summary>
        /// 上次执行时间
        /// </summary>
        public DateTime? PreviousFireTime { get; set; }
        /// <summary>
        /// 下次执行时间
        /// </summary>
        public DateTime? NextFireTime { get; set; }

        ///// <summary>
        ///// 时间间隔
        ///// </summary>
        //public string Interval { get; set; }

        /// <summary>
        /// 请求url
        /// </summary>
        public string RequestUrl { get; set; }

        /// <summary>
        /// Headers(可以包含如：Authorization授权认证)
        /// 格式：{"Authorization":"userpassword.."}
        /// </summary>
        public string Headers { get; set; }
        /// <summary>
        /// 请求参数（Post，Put请求用）
        /// {"Authorization":"userpassword.."}
        /// </summary>
        public string Parameters { get; set; }

        ///// <summary>
        ///// 任务所在类
        ///// </summary>
        //public string JobClassName { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 显示状态
        /// </summary>
        public string DisplayState
        {
            get
            {
                var state = string.Empty;
                switch (TriggerState)
                {
                    case TriggerState.Normal:
                        state = "正常";
                        break;
                    case TriggerState.Paused:
                        state = "暂停";
                        break;
                    case TriggerState.Complete:
                        state = "完成";
                        break;
                    case TriggerState.Error:
                        state = "异常";
                        break;
                    case TriggerState.Blocked:
                        state = "阻塞";
                        break;
                    case TriggerState.None:
                        state = "不存在";
                        break;
                    default:
                        state = "未知";
                        break;
                }
                return state;
            }
        }

        /// <summary>
        /// 发送类型
        /// </summary>
        public SendMsgTypeEnum? MailMsgType { get; set; } = SendMsgTypeEnum.None;
       /// <summary>
       /// 
       /// </summary>
        public string MailMsgTypeText
        {
            get
            {
                if (MailMsgType == SendMsgTypeEnum.None) return "不通知";
                else if (MailMsgType == SendMsgTypeEnum.OK) return "成功时通知";
                else if (MailMsgType == SendMsgTypeEnum.Err) return "失败时通知";
                else if (MailMsgType == SendMsgTypeEnum.All) return "所有情况通知";
                return "";
            }
        }

        /// <summary>
        /// 邮件接收者
        /// </summary>
        public List<string> SendMailers { get; set; }

        #region 发送Hork相关信息
        /// <summary>
        /// 发送类型
        /// </summary>
        public SendMsgTypeEnum? HorkMsgType { get; set; } = SendMsgTypeEnum.None;
        /// <summary>
        /// 
        /// </summary>
        public string HorkMsgTypeText
        {
            get
            {
                if (HorkMsgType == SendMsgTypeEnum.None) return "不通知";
                else if (HorkMsgType == SendMsgTypeEnum.OK) return "成功时通知";
                else if (HorkMsgType == SendMsgTypeEnum.Err) return "失败时通知";
                else if (HorkMsgType == SendMsgTypeEnum.All) return "所有情况通知";
                return "";
            }
        }
        /// <summary>
        /// Hork地址
        /// </summary>
        public string HorkUrl { get; set; }
        #endregion
        /// <summary>
        /// 
        /// </summary>
        public string AssemblyName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary> 
        public DateTime? CreateTime { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary> 
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// 是否启动 
        /// </summary> 
        public StatusEnum? Status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string StatusText
        {
            get
            {
                if (Status == StatusEnum.ENABLE) return "启用"; else if (Status == StatusEnum.DISABLE) return "禁用"; return "";
            }
        }
        ///// <summary>
        ///// 删除标志
        ///// </summary> 
        //public IsDelete? Deleted { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long? Creator { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long? Modifier { get; set; }

        /// <summary>
        /// 最后一次执行错误
        /// </summary>
        public string LastException { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int HasLastException
        {
            get
            {
                if (!string.IsNullOrEmpty(LastException)) 
                    return 1; 
                return 0;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        public void Check()
        {
            if (Id <= 0) throw new ArgumentException($"{nameof(this.Id)} 数据不能为空.");
            if (this.JobName.IsNullOrWhiteSpace()) throw new ArgumentException($"{nameof(this.JobName)} 数据不能为空.");
            if (this.JobGroup.IsNullOrWhiteSpace()) throw new ArgumentException($"{nameof(this.JobGroup)} 数据不能为空.");
        }

    }
}
