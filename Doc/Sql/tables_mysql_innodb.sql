# by: ron cordell - roncordell  20231114
#  i didn't see this anywhere, so i thought i'd post it here. this is the script from quartz to create the tables in a mysql database, modified to use innodb instead of myisam.


# make sure you have utf-8 collaction for best .net interoperability
# create database quartznet character set utf8mb4 collate utf8mb4_unicode_ci;

drop table if exists Qrtz_Fired_Triggers;
drop table if exists Qrtz_Paused_Trigger_Grps;
drop table if exists Qrtz_Scheduler_State;
drop table if exists Qrtz_Locks;
drop table if exists Qrtz_Simple_Triggers;
drop table if exists Qrtz_Simprop_Triggers;
drop table if exists Qrtz_Cron_Triggers;
drop table if exists Qrtz_Blob_Triggers;
drop table if exists Qrtz_Triggers;
drop table if exists Qrtz_Job_Details;
drop table if exists Qrtz_Calendars;

create table Qrtz_Job_Details(
Sched_Name varchar(120) not null,
Job_Name varchar(200) not null,
Job_Group varchar(200) not null,
Description varchar(250) null,
Job_Class_Name varchar(250) not null,
Is_Durable boolean not null,
Is_Nonconcurrent boolean not null,
Is_Update_Data boolean not null,
Requests_Recovery boolean not null,
Job_Data blob null,
primary key (Sched_Name,Job_Name,Job_Group))
engine=innodb;

create table Qrtz_Triggers (
Sched_Name varchar(120) not null,
Trigger_Name varchar(200) not null,
Trigger_Group varchar(200) not null,
Job_Name varchar(200) not null,
Job_Group varchar(200) not null,
Description varchar(250) null,
Next_Fire_Time bigint(19) null,
Prev_Fire_Time bigint(19) null,
Priority integer null,
Trigger_State varchar(16) not null,
Trigger_Type varchar(8) not null,
Start_Time bigint(19) not null,
End_Time bigint(19) null,
Calendar_Name varchar(200) null,
Misfire_Instr smallint(2) null,
Job_Data blob null,
primary key (Sched_Name,Trigger_Name,Trigger_Group),
foreign key (Sched_Name,Job_Name,Job_Group)
references Qrtz_Job_Details(Sched_Name,Job_Name,Job_Group))
engine=innodb;

create table Qrtz_Simple_Triggers (
Sched_Name varchar(120) not null,
Trigger_Name varchar(200) not null,
Trigger_Group varchar(200) not null,
Repeat_Count bigint(7) not null,
Repeat_Interval bigint(12) not null,
Times_Triggered bigint(10) not null,
primary key (Sched_Name,Trigger_Name,Trigger_Group),
foreign key (Sched_Name,Trigger_Name,Trigger_Group)
references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group))
engine=innodb;

create table Qrtz_Cron_Triggers (
Sched_Name varchar(120) not null,
Trigger_Name varchar(200) not null,
Trigger_Group varchar(200) not null,
Cron_Expression varchar(120) not null,
Time_Zone_Id varchar(80),
primary key (Sched_Name,Trigger_Name,Trigger_Group),
foreign key (Sched_Name,Trigger_Name,Trigger_Group)
references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group))
engine=innodb;

create table Qrtz_Simprop_Triggers
  (          
    Sched_Name varchar(120) not null,
    Trigger_Name varchar(200) not null,
    Trigger_Group varchar(200) not null,
    Str_Prop_1 varchar(512) null,
    Str_Prop_2 varchar(512) null,
    Str_Prop_3 varchar(512) null,
    Int_Prop_1 int null,
    Int_Prop_2 int null,
    Long_Prop_1 bigint null,
    Long_Prop_2 bigint null,
    Dec_Prop_1 numeric(13,4) null,
    Dec_Prop_2 numeric(13,4) null,
    Bool_Prop_1 boolean null,
    Bool_Prop_2 boolean null,
    Time_Zone_Id varchar(80) null,
    primary key (Sched_Name,Trigger_Name,Trigger_Group),
    foreign key (Sched_Name,Trigger_Name,Trigger_Group) 
    references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group))
engine=innodb;

create table Qrtz_Blob_Triggers (
Sched_Name varchar(120) not null,
Trigger_Name varchar(200) not null,
Trigger_Group varchar(200) not null,
Blob_Data blob null,
primary key (Sched_Name,Trigger_Name,Trigger_Group),
index (Sched_Name,Trigger_Name, Trigger_Group),
foreign key (Sched_Name,Trigger_Name,Trigger_Group)
references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group))
engine=innodb;

create table Qrtz_Calendars (
Sched_Name varchar(120) not null,
Calendar_Name varchar(200) not null,
Calendar blob not null,
primary key (Sched_Name,Calendar_Name))
engine=innodb;

create table Qrtz_Paused_Trigger_Grps (
Sched_Name varchar(120) not null,
Trigger_Group varchar(200) not null,
primary key (Sched_Name,Trigger_Group))
engine=innodb;

create table Qrtz_Fired_Triggers (
Sched_Name varchar(120) not null,
entry_id varchar(140) not null,
Trigger_Name varchar(200) not null,
Trigger_Group varchar(200) not null,
Instance_Name varchar(200) not null,
Fired_Time bigint(19) not null,
Sched_Time bigint(19) not null,
Priority integer not null,
State varchar(16) not null,
Job_Name varchar(200) null,
Job_Group varchar(200) null,
Is_Nonconcurrent boolean null,
Requests_Recovery boolean null,
primary key (Sched_Name,entry_id))
engine=innodb;

create table Qrtz_Scheduler_State (
Sched_Name varchar(120) not null,
Instance_Name varchar(200) not null,
Last_Checkin_Time bigint(19) not null,
Checkin_Interval bigint(19) not null,
primary key (Sched_Name,Instance_Name))
engine=innodb;

create table Qrtz_Locks (
Sched_Name varchar(120) not null,
Lock_Name varchar(40) not null,
primary key (Sched_Name,Lock_Name))
engine=innodb;

create index idx_qrtz_j_req_recovery on Qrtz_Job_Details(Sched_Name,Requests_Recovery);
create index idx_qrtz_j_grp on Qrtz_Job_Details(Sched_Name,Job_Group);

create index idx_qrtz_t_j on Qrtz_Triggers(Sched_Name,Job_Name,Job_Group);
create index idx_qrtz_t_jg on Qrtz_Triggers(Sched_Name,Job_Group);
create index idx_qrtz_t_c on Qrtz_Triggers(Sched_Name,Calendar_Name);
create index idx_qrtz_t_g on Qrtz_Triggers(Sched_Name,Trigger_Group);
create index idx_qrtz_t_state on Qrtz_Triggers(Sched_Name,Trigger_State);
create index idx_qrtz_t_n_state on Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group,Trigger_State);
create index idx_qrtz_t_n_g_state on Qrtz_Triggers(Sched_Name,Trigger_Group,Trigger_State);
create index idx_qrtz_t_next_fire_time on Qrtz_Triggers(Sched_Name,Next_Fire_Time);
create index idx_qrtz_t_nft_st on Qrtz_Triggers(Sched_Name,Trigger_State,Next_Fire_Time);
create index idx_qrtz_t_nft_misfire on Qrtz_Triggers(Sched_Name,Misfire_Instr,Next_Fire_Time);
create index idx_qrtz_t_nft_st_misfire on Qrtz_Triggers(Sched_Name,Misfire_Instr,Next_Fire_Time,Trigger_State);
create index idx_qrtz_t_nft_st_misfire_grp on Qrtz_Triggers(Sched_Name,Misfire_Instr,Next_Fire_Time,Trigger_Group,Trigger_State);

create index idx_qrtz_ft_trig_inst_name on Qrtz_Fired_Triggers(Sched_Name,Instance_Name);
create index idx_qrtz_ft_inst_job_req_rcvry on Qrtz_Fired_Triggers(Sched_Name,Instance_Name,Requests_Recovery);
create index idx_qrtz_ft_j_g on Qrtz_Fired_Triggers(Sched_Name,Job_Name,Job_Group);
create index idx_qrtz_ft_jg on Qrtz_Fired_Triggers(Sched_Name,Job_Group);
create index idx_qrtz_ft_t_g on Qrtz_Fired_Triggers(Sched_Name,Trigger_Name,Trigger_Group);
create index idx_qrtz_ft_tg on Qrtz_Fired_Triggers(Sched_Name,Trigger_Group);

commit; 
