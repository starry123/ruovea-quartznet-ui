-- this script is for firebird 20231114
-- firebird db script submitted by leonardo alves and tweaked by siyu wu

drop table Qrtz_Fired_Triggers;
drop table Qrtz_Paused_Trigger_Grps;
drop table Qrtz_Scheduler_State;
drop table Qrtz_Locks;
drop table Qrtz_Simple_Triggers;
drop table Qrtz_Simprop_Triggers;
drop table Qrtz_Cron_Triggers;
drop table Qrtz_Blob_Triggers;
drop table Qrtz_Triggers;
drop table Qrtz_Job_Details;
drop table Qrtz_Calendars;


create table Qrtz_Job_Details (
    Sched_Name         varchar(120) not null,
    Job_Name           varchar(150) not null,
    Job_Group          varchar(150) not null,
    Description        varchar(250) default null,
    Job_Class_Name     varchar(250) not null,
    Is_Durable         smallint not null,
    Is_Nonconcurrent   smallint not null,
    Is_Update_Data     smallint not null,
    Requests_Recovery  smallint not null,
    Job_Data           blob default null,
    constraint pk_Qrtz_Job_Details primary key (Sched_Name,Job_Name,Job_Group)
);

create table Qrtz_Triggers (
    Sched_Name      varchar(120) not null,
    Trigger_Name    varchar(150) not null,
    Trigger_Group   varchar(150) not null,
    Job_Name        varchar(150) not null,
    Job_Group       varchar(150) not null,
    Description     varchar(250) default null,
    Next_Fire_Time  bigint default null,
    Prev_Fire_Time  bigint default null,
    Priority        integer default null,
    Trigger_State   varchar(16) not null,
    Trigger_Type    varchar(8) not null,
    Start_Time      bigint not null,
    End_Time        bigint default null,
    Calendar_Name   varchar(200) default null,
    Misfire_Instr   smallint default null,
    Job_Data        blob default null,
    constraint pk_Qrtz_Triggers primary key (Sched_Name, Trigger_Name, Trigger_Group),
    constraint fk_Qrtz_Triggers_1 foreign key (Sched_Name, Job_Name, Job_Group)
    references Qrtz_Job_Details(Sched_Name, Job_Name, Job_Group)
);

create table Qrtz_Simple_Triggers (
    Sched_Name       varchar(120) not null,
    Trigger_Name     varchar(150) not null,
    Trigger_Group    varchar(150) not null,
    Repeat_Count     bigint not null,
    Repeat_Interval  bigint not null,
    Times_Triggered  bigint not null,
    constraint pk_Qrtz_Simple_Triggers primary key (Sched_Name, Trigger_Name, Trigger_Group),
    constraint fk_Qrtz_Simple_Triggers_1 foreign key (Sched_Name, Trigger_Name, Trigger_Group)
    references Qrtz_Triggers(Sched_Name, Trigger_Name, Trigger_Group)
);

create table Qrtz_Cron_Triggers (
    Sched_Name       varchar(120) not null,
    Trigger_Name     varchar(150) not null,
    Trigger_Group    varchar(150) not null,
    Cron_Expression  varchar(250) not null,
    Time_Zone_Id     varchar(80),
    constraint pk_Qrtz_Cron_Triggers primary key (Sched_Name, Trigger_Name, Trigger_Group),
    constraint fk_Qrtz_Cron_Triggers_1 foreign key (Sched_Name, Trigger_Name, Trigger_Group)
    references Qrtz_Triggers(Sched_Name, Trigger_Name,Trigger_Group)
);

create table Qrtz_Simprop_Triggers (
    Sched_Name     varchar(120) not null,
    Trigger_Name   varchar(150) not null,
    Trigger_Group  varchar(150) not null,
    Str_Prop_1     varchar(512) default null,
    Str_Prop_2     varchar(512) default null,
    Str_Prop_3     varchar(512) default null,
    Int_Prop_1     integer default null,
    Int_Prop_2     integer default null,
    Long_Prop_1    bigint default null,
    Long_Prop_2    bigint default null,
    Dec_Prop_1     numeric(9,0) default  null,
    Dec_Prop_2     numeric(9,0) default null,
    Bool_Prop_1    smallint default null,
    Bool_Prop_2    smallint default null,
    Time_Zone_Id   varchar(80) default null,
    constraint pk_Qrtz_Simprop_Triggers primary key (Sched_Name, Trigger_Name, Trigger_Group),
    constraint fk_Qrtz_Simprop_Triggers_1 foreign key (Sched_Name, Trigger_Name, Trigger_Group)
    references Qrtz_Triggers(Sched_Name, Trigger_Name,Trigger_Group)
);

create table Qrtz_Blob_Triggers (
    Sched_Name     varchar(120) not null,
    Trigger_Name   varchar(150) not null,
    Trigger_Group  varchar(150) not null,
    Blob_Data      blob default null,
    constraint pk_Qrtz_Blob_Triggers primary key (Sched_Name, Trigger_Name,Trigger_Group),
    constraint fk_Qrtz_Blob_Triggers_1 foreign key (Sched_Name, Trigger_Name,Trigger_Group)
    references Qrtz_Triggers(Sched_Name, Trigger_Name,Trigger_Group)
);

create table Qrtz_Calendars (
    Sched_Name     varchar(120) not null,
    Calendar_Name  varchar(200) not null,
    Calendar       blob not null,
    constraint pk_Qrtz_Calendars primary key (Sched_Name, Calendar_Name)
);

create table Qrtz_Paused_Trigger_Grps (
    Sched_Name     varchar(120) not null,
    Trigger_Group  varchar(150) not null,
    constraint pk_Qrtz_Paused_Trigger_Grps primary key (Sched_Name, Trigger_Group)
);

create table Qrtz_Fired_Triggers (
    Sched_Name         varchar(120) not null,
    entry_id           varchar(140) not null,
    Trigger_Name       varchar(150) not null,
    Trigger_Group      varchar(150) not null,
    Instance_Name      varchar(200) not null,
    Fired_Time         bigint not null,
    Sched_Time         bigint not null,
    Priority           integer not null,
    State              varchar(16) not null,
    Job_Name           varchar(150) default null,
    Job_Group          varchar(150) default null,
    Is_Nonconcurrent   smallint not null,
    Requests_Recovery  smallint default null,
    constraint pk_Qrtz_Fired_Triggers primary key (Sched_Name, entry_id)
);

create table Qrtz_Scheduler_State (
    Sched_Name         varchar(120) not null,
    Instance_Name      varchar(200) not null,
    Last_Checkin_Time  bigint not null,
    Checkin_Interval   bigint not null,
    constraint pk_Qrtz_Scheduler_State primary key (Sched_Name, Instance_Name)
);

create table Qrtz_Locks (
    Sched_Name  varchar(120) not null,
    Lock_Name   varchar(40) not null,
    constraint pk_Qrtz_Locks primary key (Sched_Name, Lock_Name)
);

create index idx_qrtz_j_req_recovery on Qrtz_Job_Details(Sched_Name,Requests_Recovery);
create index idx_qrtz_j_grp on Qrtz_Job_Details(Sched_Name,Job_Group);

create index idx_qrtz_t_j on Qrtz_Triggers(Sched_Name,Job_Name,Job_Group);
create index idx_qrtz_t_jg on Qrtz_Triggers(Sched_Name,Job_Group);
create index idx_qrtz_t_c on Qrtz_Triggers(Sched_Name,Calendar_Name);
create index idx_qrtz_t_g on Qrtz_Triggers(Sched_Name,Trigger_Group);
create index idx_qrtz_t_state on Qrtz_Triggers(Sched_Name,Trigger_State);
create index idx_qrtz_t_n_state on Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group,Trigger_State);
create index idx_qrtz_t_n_g_state on Qrtz_Triggers(Sched_Name,Trigger_Group,Trigger_State);
create index idx_qrtz_t_next_fire_time on Qrtz_Triggers(Sched_Name,Next_Fire_Time);
create index idx_qrtz_t_nft_st on Qrtz_Triggers(Sched_Name,Trigger_State,Next_Fire_Time);
create index idx_qrtz_t_nft_misfire on Qrtz_Triggers(Sched_Name,Misfire_Instr,Next_Fire_Time);
create index idx_qrtz_t_nft_st_misfire on Qrtz_Triggers(Sched_Name,Misfire_Instr,Next_Fire_Time,Trigger_State);
create index idx_qrtz_t_nft_st_misfire_grp on Qrtz_Triggers(Sched_Name,Misfire_Instr,Next_Fire_Time,Trigger_Group,Trigger_State);

create index idx_qrtz_ft_trig_inst_name on Qrtz_Fired_Triggers(Sched_Name,Instance_Name);
create index idx_qrtz_ft_inst_job_req_rcvry on Qrtz_Fired_Triggers(Sched_Name,Instance_Name,Requests_Recovery);
create index idx_qrtz_ft_j_g on Qrtz_Fired_Triggers(Sched_Name,Job_Name,Job_Group);
create index idx_qrtz_ft_jg on Qrtz_Fired_Triggers(Sched_Name,Job_Group);
create index idx_qrtz_ft_t_g on Qrtz_Fired_Triggers(Sched_Name,Trigger_Name,Trigger_Group);
create index idx_qrtz_ft_tg on Qrtz_Fired_Triggers(Sched_Name,Trigger_Group);

commit;
