﻿using RuoVea.ExDto;
using RuoVea.ExEnum;
using SqlSugar;

namespace RuoVea.QuartzNetUI.Entitys
{
    /// <summary>
    /// 任务调度日志
    /// </summary>
    [SugarTable("Qrtz_Task_Log", tableDescription: "任务调度日志")]
    public class TaskLog : KeyEntity, DeletedEntity
    {
        /// <summary>
        /// 主键
        /// </summary>
        [SugarColumn(ColumnName = "Id", IsPrimaryKey = true, ColumnDescription = "主键")]
        public long Id { get; set; }

        /// <summary>
        /// 任务主键 
        /// </summary>
        [SugarColumn(ColumnName = "Job_Id", ColumnDescription = "任务主键")]
        public long? JobId { get; set; }

        /// <summary>
        /// 调度名称 
        /// </summary>
        [SugarColumn(ColumnName = "Sched_Name", IsNullable = true, Length = 120, ColumnDescription = "调度名称")]
        public string SchedName { get; set; }

        /// <summary>
        /// 任务名称
        /// </summary>
        [SugarColumn(ColumnName = "Job_Name", IsNullable = true, ColumnDescription = "任务名称")]
        public string JobName { get; set; }

        /// <summary>
        /// 任务组名称
        /// </summary>
        [SugarColumn(ColumnName = "Job_Group", IsNullable = true, ColumnDescription = "任务组名称")]
        public string JobGroup { get; set; }

        /// <summary>
        /// 开始执行时间
        /// </summary>
        [SugarColumn(ColumnName = "Begin_Time", IsNullable = true, ColumnDescription = "开始执行时间")]
        public string BeginTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        [SugarColumn(ColumnName = "End_Time", IsNullable = true, ColumnDescription = "结束时间")]
        public string EndTime { get; set; }

        /// <summary>
        /// 执行成功状态
        /// </summary>
        [SugarColumn(ColumnName = "Status", ColumnDescription = "执行成功状态")]
        public ExEnum.YesOrNot Status { get; set; }
        /// <summary>
        /// 耗时（秒）
        /// </summary>
        [SugarColumn(ColumnName = "Elapsed", IsNullable = true, ColumnDescription = " 耗时（秒）")]
        public double Elapsed { get; set; }

        /// <summary>
        /// 请求地址/任务程序集地址
        /// </summary>
        [SugarColumn(ColumnName = "Job_Address", IsNullable = true, ColumnDescription = "请求地址/任务程序集地址")]
        public string JobAddress { get; set; }

        /// <summary>
        /// 请求类型
        /// </summary>
        [SugarColumn(ColumnName = "Run_Type", ColumnDescription = "请求类型")]
        public string RunType { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        [SugarColumn(ColumnName = "Parameters", IsNullable = true, Length = 4000, ColumnDescription = "请求参数")]
        public string Parameters { get; set; }

        /// <summary>
        /// 请求结果
        /// </summary>
        [SugarColumn(ColumnName = "Result", IsNullable = true, Length = 4000, ColumnDescription = "请求结果")]
        public string Result { get; set; }

        /// <summary>
        /// 异常消息
        /// </summary>
        [SugarColumn(ColumnName = "Exception", IsNullable = true, Length = 4000, ColumnDescription = "异常消息")]
        public string Exception { get; set; }

        /// <summary>
        /// 执行时间
        /// </summary>
        [SugarColumn(ColumnName = "Create_Time", ColumnDescription = "执行时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 删除标志
        /// </summary>
        [SugarColumn(ColumnName = "Is_Delete", ColumnDescription = "删除标志")]
        public IsDelete? Deleted { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [SugarColumn(ColumnName = "Remark", Length = 4000, IsNullable = true, ColumnDescription = "备注")]
        public string Remark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public void Insert()
        {
            CreateTime = DateTime.Now;
            Deleted = 0;
        }
    }
}

