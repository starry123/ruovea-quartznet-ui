﻿using RuoVea.ExEnum;

namespace RuoVea.QuartzNetUI.Server.Dto
{
    /// <summary>
    /// 
    /// </summary>
    public class TaskDetailQueryDto
    {
        /// <summary>
        /// 任务名称
        /// </summary>
        public string JobName { get; set; }

        /// <summary>
        /// 任务分组
        /// </summary>
        public string JobGroup { get; set; }

        /// <summary>
        /// 是否启动
        /// </summary>
        public StatusEnum? Status { get; set; }
    }

}
