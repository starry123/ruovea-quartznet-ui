--  20231114
-- a hint submitted by a user: oracle db must be created as "shared" and the 
-- job_queue_processes parameter  must be greater than 2
-- however, these settings are pretty much standard after any
-- oracle install, so most users need not worry about this.
--
-- many other users (including the primary author of quartz) have had success
-- running in dedicated mode, so only consider the above as a hint ;-)
--

delete from Qrtz_Fired_Triggers;
delete from Qrtz_Simple_Triggers;
delete from Qrtz_Simprop_Triggers;
delete from Qrtz_Cron_Triggers;
delete from Qrtz_Blob_Triggers;
delete from Qrtz_Triggers;
delete from Qrtz_Job_Details;
delete from Qrtz_Calendars;
delete from Qrtz_Paused_Trigger_Grps;
delete from Qrtz_Locks;
delete from Qrtz_Scheduler_State;

drop table Qrtz_Calendars;
drop table Qrtz_Fired_Triggers;
drop table Qrtz_Blob_Triggers;
drop table Qrtz_Cron_Triggers;
drop table Qrtz_Simple_Triggers;
drop table Qrtz_Simprop_Triggers;
drop table Qrtz_Triggers;
drop table Qrtz_Job_Details;
drop table Qrtz_Paused_Trigger_Grps;
drop table Qrtz_Locks;
drop table Qrtz_Scheduler_State;


create table Qrtz_Job_Details
  (
    Sched_Name varchar2(120) not null,
    Job_Name  varchar2(200) not null,
    Job_Group varchar2(200) not null,
    Description varchar2(250) null,
    Job_Class_Name   varchar2(250) not null, 
    Is_Durable varchar2(1) not null,
    Is_Nonconcurrent varchar2(1) not null,
    Is_Update_Data varchar2(1) not null,
    Requests_Recovery varchar2(1) not null,
    Job_Data blob null,
    constraint Qrtz_Job_Details_pk primary key (Sched_Name,Job_Name,Job_Group)
);
create table Qrtz_Triggers
  (
    Sched_Name varchar2(120) not null,
    Trigger_Name varchar2(200) not null,
    Trigger_Group varchar2(200) not null,
    Job_Name  varchar2(200) not null, 
    Job_Group varchar2(200) not null,
    Description varchar2(250) null,
    Next_Fire_Time number(19) null,
    Prev_Fire_Time number(19) null,
    Priority number(13) null,
    Trigger_State varchar2(16) not null,
    Trigger_Type varchar2(8) not null,
    Start_Time number(19) not null,
    End_Time number(19) null,
    Calendar_Name varchar2(200) null,
    Misfire_Instr number(2) null,
    Job_Data blob null,
    constraint Qrtz_Triggers_pk primary key (Sched_Name,Trigger_Name,Trigger_Group),
    constraint qrtz_trigger_to_jobs_fk foreign key (Sched_Name,Job_Name,Job_Group) 
      references Qrtz_Job_Details(Sched_Name,Job_Name,Job_Group) 
);
create table Qrtz_Simple_Triggers
  (
    Sched_Name varchar2(120) not null,
    Trigger_Name varchar2(200) not null,
    Trigger_Group varchar2(200) not null,
    Repeat_Count number(7) not null,
    Repeat_Interval number(12) not null,
    Times_Triggered number(10) not null,
    constraint qrtz_simple_trig_pk primary key (Sched_Name,Trigger_Name,Trigger_Group),
    constraint qrtz_simple_trig_to_trig_fk foreign key (Sched_Name,Trigger_Name,Trigger_Group) 
	references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group)
);
create table Qrtz_Cron_Triggers
  (
    Sched_Name varchar2(120) not null,
    Trigger_Name varchar2(200) not null,
    Trigger_Group varchar2(200) not null,
    Cron_Expression varchar2(120) not null,
    Time_Zone_Id varchar2(80),
    constraint qrtz_cron_trig_pk primary key (Sched_Name,Trigger_Name,Trigger_Group),
    constraint qrtz_cron_trig_to_trig_fk foreign key (Sched_Name,Trigger_Name,Trigger_Group) 
      references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group)
);
create table Qrtz_Simprop_Triggers
  (          
    Sched_Name varchar2(120) not null,
    Trigger_Name varchar2(200) not null,
    Trigger_Group varchar2(200) not null,
    Str_Prop_1 varchar2(512) null,
    Str_Prop_2 varchar2(512) null,
    Str_Prop_3 varchar2(512) null,
    Int_Prop_1 number(10) null,
    Int_Prop_2 number(10) null,
    Long_Prop_1 number(19) null,
    Long_Prop_2 number(19) null,
    Dec_Prop_1 numeric(13,4) null,
    Dec_Prop_2 numeric(13,4) null,
    Bool_Prop_1 varchar2(1) null,
    Bool_Prop_2 varchar2(1) null,
    Time_Zone_Id varchar2(80) null,
    constraint qrtz_simprop_trig_pk primary key (Sched_Name,Trigger_Name,Trigger_Group),
    constraint qrtz_simprop_trig_to_trig_fk foreign key (Sched_Name,Trigger_Name,Trigger_Group) 
      references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group)
);
create table Qrtz_Blob_Triggers
  (
    Sched_Name varchar2(120) not null,
    Trigger_Name varchar2(200) not null,
    Trigger_Group varchar2(200) not null,
    Blob_Data blob null,
    constraint qrtz_blob_trig_pk primary key (Sched_Name,Trigger_Name,Trigger_Group),
    constraint qrtz_blob_trig_to_trig_fk foreign key (Sched_Name,Trigger_Name,Trigger_Group) 
        references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group)
);
create table Qrtz_Calendars
  (
    Sched_Name varchar2(120) not null,
    Calendar_Name  varchar2(200) not null, 
    Calendar blob not null,
    constraint Qrtz_Calendars_pk primary key (Sched_Name,Calendar_Name)
);
create table Qrtz_Paused_Trigger_Grps
  (
    Sched_Name varchar2(120) not null,
    Trigger_Group  varchar2(200) not null, 
    constraint qrtz_paused_trig_grps_pk primary key (Sched_Name,Trigger_Group)
);
create table Qrtz_Fired_Triggers 
  (
    Sched_Name varchar2(120) not null,
    entry_id varchar2(140) not null,
    Trigger_Name varchar2(200) not null,
    Trigger_Group varchar2(200) not null,
    Instance_Name varchar2(200) not null,
    Fired_Time number(19) not null,
    Sched_Time number(19) not null,
	Priority number(13) not null,
    State varchar2(16) not null,
    Job_Name varchar2(200) null,
    Job_Group varchar2(200) null,
    Is_Nonconcurrent varchar2(1) null,
    Requests_Recovery varchar2(1) null,
    constraint qrtz_fired_trigger_pk primary key (Sched_Name,entry_id)
);
create table Qrtz_Scheduler_State 
  (
    Sched_Name varchar2(120) not null,
    Instance_Name varchar2(200) not null,
    Last_Checkin_Time number(19) not null,
    Checkin_Interval number(13) not null,
    constraint Qrtz_Scheduler_State_pk primary key (Sched_Name,Instance_Name)
);
create table Qrtz_Locks
  (
    Sched_Name varchar2(120) not null,
    Lock_Name  varchar2(40) not null, 
    constraint Qrtz_Locks_pk primary key (Sched_Name,Lock_Name)
);

create index idx_qrtz_j_req_recovery on Qrtz_Job_Details(Sched_Name,Requests_Recovery);
create index idx_qrtz_j_grp on Qrtz_Job_Details(Sched_Name,Job_Group);

create index idx_qrtz_t_j on Qrtz_Triggers(Sched_Name,Job_Name,Job_Group);
create index idx_qrtz_t_jg on Qrtz_Triggers(Sched_Name,Job_Group);
create index idx_qrtz_t_c on Qrtz_Triggers(Sched_Name,Calendar_Name);
create index idx_qrtz_t_g on Qrtz_Triggers(Sched_Name,Trigger_Group);
create index idx_qrtz_t_state on Qrtz_Triggers(Sched_Name,Trigger_State);
create index idx_qrtz_t_n_state on Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group,Trigger_State);
create index idx_qrtz_t_n_g_state on Qrtz_Triggers(Sched_Name,Trigger_Group,Trigger_State);
create index idx_qrtz_t_next_fire_time on Qrtz_Triggers(Sched_Name,Next_Fire_Time);
create index idx_qrtz_t_nft_st on Qrtz_Triggers(Sched_Name,Trigger_State,Next_Fire_Time);
create index idx_qrtz_t_nft_misfire on Qrtz_Triggers(Sched_Name,Misfire_Instr,Next_Fire_Time);
create index idx_qrtz_t_nft_st_misfire on Qrtz_Triggers(Sched_Name,Misfire_Instr,Next_Fire_Time,Trigger_State);
create index idx_qrtz_t_nft_st_misfire_grp on Qrtz_Triggers(Sched_Name,Misfire_Instr,Next_Fire_Time,Trigger_Group,Trigger_State);

create index idx_qrtz_ft_trig_inst_name on Qrtz_Fired_Triggers(Sched_Name,Instance_Name);
create index idx_qrtz_ft_inst_job_req_rcvry on Qrtz_Fired_Triggers(Sched_Name,Instance_Name,Requests_Recovery);
create index idx_qrtz_ft_j_g on Qrtz_Fired_Triggers(Sched_Name,Job_Name,Job_Group);
create index idx_qrtz_ft_jg on Qrtz_Fired_Triggers(Sched_Name,Job_Group);
create index idx_qrtz_ft_t_g on Qrtz_Fired_Triggers(Sched_Name,Trigger_Name,Trigger_Group);
create index idx_qrtz_ft_tg on Qrtz_Fired_Triggers(Sched_Name,Trigger_Group);


