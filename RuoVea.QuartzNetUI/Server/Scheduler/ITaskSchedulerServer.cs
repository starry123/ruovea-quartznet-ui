﻿using RuoVea.ExDto;
using RuoVea.QuartzNetUI.Server.Dto;

namespace RuoVea.QuartzNetUI.Server.Scheduler
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITaskSchedulerServer
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<RestfulResult> StartTaskScheduleAsync();
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<RestfulResult> StopTaskScheduleAsync();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        bool CheckExists(TaskDetailInDto data);
/// <summary>
/// 
/// </summary>
/// <param name="data"></param>
/// <returns></returns>
        Task<RestfulResult> AddTaskScheduleAsync(TaskDetailInDto data);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<RestfulResult> PauseTaskScheduleAsync(TaskDetailInDto data);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<RestfulResult> ResumeTaskScheduleAsync(TaskDetailInDto data);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<RestfulResult> DeleteTaskScheduleAsync(TaskDetailInDto data);

        /// <summary>
        /// 立即运行
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<RestfulResult> RunTaskScheduleAsync(TaskDetailInDto data);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        RestfulResult ValidateTaskDetail(TaskDetailInDto data);

        /// <summary>
        /// 更新计划任务
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<RestfulResult> UpdateTaskScheduleAsync(TaskDetailInDto data);

    }
}
