﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using RuoVea.QuartzNetUI.Authors.Basic;
using RuoVea.QuartzNetUI.Authors.Language;

namespace RuoVea.QuartzNetUI.Authors
{
    /// <summary>
    /// 
    /// </summary>
    public static class TasksServiceCollectionExtensions
    {
        const string realm= "qazwsxedcrfvtgbbyhnumjiklop";
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static IServiceCollection AddQuartzNetAuthorServer(this IServiceCollection services, IConfiguration config)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));
            if (config == null) throw new ArgumentNullException(I18n.AddSettingInfo);
            services.Configure<QuartzNetUIAuthorOption>(config);
            QuartzNetUIAuthorOption option = services.BuildServiceProvider().GetService<IOptions<QuartzNetUIAuthorOption>>()?.Value;

            if (option == null)
                throw new ArgumentNullException(I18n.AddSettingInfo);

            services.AddQuartzNetServer(config );

            services.AddAuthentication(BasicAuthenticationScheme.DefaultScheme)
                .AddScheme<BasicAuthenticationOption, BasicAuthenticationHandler>(BasicAuthenticationScheme.DefaultScheme,
                option =>
                {
                    option.Realm = realm;
                    option.UserName = option.UserName;
                    option.UserPwd = option.UserPwd;
                });
            return services;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static IServiceCollection AddQuartzNetAuthorServer(this IServiceCollection services, Action<QuartzNetUIAuthorOption> config)
        {
            if (services == null)
                throw new ArgumentNullException("services");

            if (config == null)
                throw new ArgumentNullException(I18n.AddSettingInfo);
            services.Configure(config);

            QuartzNetUIAuthorOption option= services.BuildServiceProvider().GetService<IOptions<QuartzNetUIAuthorOption>>()?.Value;

            if (option == null)
                throw new ArgumentNullException(I18n.AddSettingInfo);

            services.AddQuartzNetServer(config =>
            {
                config.ConnectionString = option.ConnectionString;
                config.RootUrl = option.RootUrl;
                config.DbType = option.DbType;
            });

            services.AddAuthentication(BasicAuthenticationScheme.DefaultScheme).AddScheme<BasicAuthenticationOption, BasicAuthenticationHandler>(BasicAuthenticationScheme.DefaultScheme,
                option =>
                {
                    option.Realm = realm;
                    option.UserName = option.UserName;
                    option.UserPwd = option.UserPwd;
                });
            return services;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseQuartzAuthorUI(this IApplicationBuilder app)
        {
            app.UseQuartzUI(); 
            QuartzNetUIAuthorOption option = app.ApplicationServices.GetRequiredService<IOptions<QuartzNetUIAuthorOption>>()?.Value;
            string authorUrl = option?.AuthorUrl ?? option.RootUrl;
            app.UseWhen(
                predicate: x => x.Request.Path.StartsWithSegments(new PathString("/" + authorUrl)),
                configuration: appBuilder => { appBuilder.UseMiddleware<BasicAuthenticationMiddleware>(); }
                );
            return app;
        }
    }
}
