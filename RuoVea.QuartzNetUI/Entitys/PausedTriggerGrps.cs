﻿using SqlSugar;
namespace RuoVea.QuartzNetUI.Entitys
{
    /// <summary>
    /// 存储已暂停的 Trigger 组的信息
    ///</summary>
    [SugarTable("Qrtz_Paused_Trigger_Grps", tableDescription: "存储已暂停的Trigger组的信息")]
    public class PausedTriggerGrps
    {
        /// <summary>
        /// 调度名称 
        ///</summary>
        [SugarColumn(ColumnName = "Sched_Name", Length = 120, IsPrimaryKey = true, ColumnDescription = "调度名称")]
        public string SchedName { get; set; }
        /// <summary>
        /// qrtz_triggers表trigger_group的外键 
        ///</summary>
        [SugarColumn(ColumnName = "Trigger_Group", Length = 200, IsPrimaryKey = true, ColumnDescription = "qrtz_triggers表trigger_group的外键")]
        public string TriggerGroup { get; set; }
    }
}
