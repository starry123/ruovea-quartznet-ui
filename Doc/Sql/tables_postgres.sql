drop table if exists Qrtz_Fired_Triggers; 
drop table if exists Qrtz_Paused_Trigger_Grps;
drop table if exists Qrtz_Scheduler_State;
drop table if exists Qrtz_Locks;
drop table if exists Qrtz_Simprop_Triggers;
drop table if exists Qrtz_Simple_Triggers;
drop table if exists Qrtz_Cron_Triggers;
drop table if exists Qrtz_Blob_Triggers;
drop table if exists Qrtz_Triggers;
drop table if exists Qrtz_Job_Details;
drop table if exists Qrtz_Calendars;


create table Qrtz_Job_Details
  (
    Sched_Name varchar(120) not null,
	Job_Name  varchar(200) not null,
    Job_Group varchar(200) not null,
    Description varchar(250) null,
    Job_Class_Name   varchar(250) not null, 
    Is_Durable bool not null,
    Is_Nonconcurrent bool not null,
    Is_Update_Data bool not null,
	Requests_Recovery bool not null,
    Job_Data bytea null,
    primary key (Sched_Name,Job_Name,Job_Group)
);

create table Qrtz_Triggers
  (
    Sched_Name varchar(120) not null,
	Trigger_Name varchar(150) not null,
    Trigger_Group varchar(150) not null,
    Job_Name  varchar(200) not null, 
    Job_Group varchar(200) not null,
    Description varchar(250) null,
    Next_Fire_Time bigint null,
    Prev_Fire_Time bigint null,
    Priority integer null,
    Trigger_State varchar(16) not null,
    Trigger_Type varchar(8) not null,
    Start_Time bigint not null,
    End_Time bigint null,
    Calendar_Name varchar(200) null,
    Misfire_Instr smallint null,
    Job_Data bytea null,
    primary key (Sched_Name,Trigger_Name,Trigger_Group),
    foreign key (Sched_Name,Job_Name,Job_Group) 
		references Qrtz_Job_Details(Sched_Name,Job_Name,Job_Group) 
);

create table Qrtz_Simple_Triggers
  (
    Sched_Name varchar(120) not null,
	Trigger_Name varchar(150) not null,
    Trigger_Group varchar(150) not null,
    Repeat_Count bigint not null,
    Repeat_Interval bigint not null,
    Times_Triggered bigint not null,
    primary key (Sched_Name,Trigger_Name,Trigger_Group),
    foreign key (Sched_Name,Trigger_Name,Trigger_Group) 
		references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group) on delete cascade
);

create table Qrtz_Simprop_Triggers 
  (
    Sched_Name varchar (120) not null,
    Trigger_Name varchar (150) not null ,
    Trigger_Group varchar (150) not null ,
    Str_Prop_1 varchar (512) null,
    Str_Prop_2 varchar (512) null,
    Str_Prop_3 varchar (512) null,
    Int_Prop_1 integer null,
    Int_Prop_2 integer null,
    Long_Prop_1 bigint null,
    Long_Prop_2 bigint null,
    Dec_Prop_1 numeric null,
    Dec_Prop_2 numeric null,
    Bool_Prop_1 bool null,
    Bool_Prop_2 bool null,
	Time_Zone_Id varchar(80) null,
	primary key (Sched_Name,Trigger_Name,Trigger_Group),
    foreign key (Sched_Name,Trigger_Name,Trigger_Group) 
		references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group) on delete cascade
);

create table Qrtz_Cron_Triggers
  (
    Sched_Name varchar (120) not null,
    Trigger_Name varchar(150) not null,
    Trigger_Group varchar(150) not null,
    Cron_Expression varchar(250) not null,
    Time_Zone_Id varchar(80),
    primary key (Sched_Name,Trigger_Name,Trigger_Group),
    foreign key (Sched_Name,Trigger_Name,Trigger_Group) 
		references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group) on delete cascade
);

create table Qrtz_Blob_Triggers
  (
    Sched_Name varchar (120) not null,
    Trigger_Name varchar(150) not null,
    Trigger_Group varchar(150) not null,
    Blob_Data bytea null,
    primary key (Sched_Name,Trigger_Name,Trigger_Group),
    foreign key (Sched_Name,Trigger_Name,Trigger_Group) 
		references Qrtz_Triggers(Sched_Name,Trigger_Name,Trigger_Group) on delete cascade
);

create table Qrtz_Calendars
  (
    Sched_Name varchar (120) not null,
    Calendar_Name  varchar(200) not null, 
    Calendar bytea not null,
    primary key (Sched_Name,Calendar_Name)
);

create table Qrtz_Paused_Trigger_Grps
  (
    Sched_Name varchar (120) not null,
    Trigger_Group varchar(150) not null, 
    primary key (Sched_Name,Trigger_Group)
);

create table Qrtz_Fired_Triggers 
  (
    Sched_Name varchar (120) not null,
    entry_id varchar(140) not null,
    Trigger_Name varchar(150) not null,
    Trigger_Group varchar(150) not null,
    Instance_Name varchar(200) not null,
    Fired_Time bigint not null,
	Sched_Time bigint not null,
    Priority integer not null,
    State varchar(16) not null,
    Job_Name varchar(200) null,
    Job_Group varchar(200) null,
    Is_Nonconcurrent bool not null,
    Requests_Recovery bool null,
    primary key (Sched_Name,entry_id)
);

create table Qrtz_Scheduler_State 
  (
    Sched_Name varchar (120) not null,
    Instance_Name varchar(200) not null,
    Last_Checkin_Time bigint not null,
    Checkin_Interval bigint not null,
    primary key (Sched_Name,Instance_Name)
);

create table Qrtz_Locks
  (
    Sched_Name varchar (120) not null,
    Lock_Name  varchar(40) not null, 
    primary key (Sched_Name,Lock_Name)
);

create index idx_qrtz_j_req_recovery on Qrtz_Job_Details(Requests_Recovery);
create index idx_qrtz_t_next_fire_time on Qrtz_Triggers(Next_Fire_Time);
create index idx_qrtz_t_state on Qrtz_Triggers(Trigger_State);
create index idx_qrtz_t_nft_st on Qrtz_Triggers(Next_Fire_Time,Trigger_State);
create index idx_qrtz_ft_trig_name on Qrtz_Fired_Triggers(Trigger_Name);
create index idx_qrtz_ft_trig_group on Qrtz_Fired_Triggers(Trigger_Group);
create index idx_qrtz_ft_trig_nm_gp on Qrtz_Fired_Triggers(Sched_Name,Trigger_Name,Trigger_Group);
create index idx_qrtz_ft_trig_inst_name on Qrtz_Fired_Triggers(Instance_Name);
create index idx_qrtz_ft_Job_Name on Qrtz_Fired_Triggers(Job_Name);
create index idx_qrtz_ft_Job_Group on Qrtz_Fired_Triggers(Job_Group);
create index idx_qrtz_ft_job_req_recovery on Qrtz_Fired_Triggers(Requests_Recovery);